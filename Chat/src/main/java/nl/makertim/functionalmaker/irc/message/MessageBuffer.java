package nl.makertim.functionalmaker.irc.message;

public class MessageBuffer {
	private StringBuilder buffer;

	public MessageBuffer() {
		buffer = new StringBuilder();
	}

	public void append(byte[] bytes) {
		buffer.append(new String(bytes));
	}

	public boolean hasCompleteMessage() {
		return buffer.toString().contains("\r\n");
	}

	public String getNextMessage() {
		int index = buffer.indexOf("\r\n");
		String message = "";

		if (index > -1) {
			message = buffer.substring(0, index);
			buffer = new StringBuilder(buffer.substring(index + 2));
		}

		return message;
	}
}
