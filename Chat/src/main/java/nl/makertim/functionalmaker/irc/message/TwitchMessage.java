package nl.makertim.functionalmaker.irc.message;

import nl.makertim.functionalmaker.Try;
import nl.makertim.functionalmaker.irc.irc.Command;
import nl.makertim.functionalmaker.property.IntProperty;

import java.util.Arrays;
import java.util.Map;

public class TwitchMessage extends Message {

	private String rawTags;
	private Map<String, String> rawTagsMap;
	private Badge[] badges;
	private int bits;
	private String color;
	private String displayName;
	private String userId;
	private String messageId;
	private String roomId;
	private String type;
	private String language;
	private boolean mod;
	private boolean isSub;
	private boolean isTurbo;


	public TwitchMessage() {
	}

	public TwitchMessage(String raw, String origin, String nickname, String rawCommand, Command command, String target, String content, String rawTags) {
		super(raw, origin, nickname, rawCommand, command, target, content);
		if (!rawTags.startsWith("@")) {
			rawTags = '@' + rawTags;
		}
		new TwitchMessageParser().parsePre(this, new String[]{rawTags}, new IntProperty(0));
	}

	public String getRawTags() {
		return rawTags;
	}

	public void setRawTags(String rawTags) {
		this.rawTags = rawTags;
	}

	public Badge[] getBadges() {
		return badges;
	}

	public void setBadges(Badge[] badges) {
		this.badges = badges;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isMod() {
		return mod;
	}

	public void setMod(boolean mod) {
		this.mod = mod;
	}

	public boolean isSub() {
		return isSub;
	}

	public void setSub(boolean sub) {
		isSub = sub;
	}

	public boolean isTurbo() {
		return isTurbo;
	}

	public void setTurbo(boolean turbo) {
		isTurbo = turbo;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public int getBits() {
		return bits;
	}

	public void setBits(int bits) {
		this.bits = bits;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setRawTagsMap(Map<String, String> rawTagsMap) {
		this.rawTagsMap = rawTagsMap;
	}

	public Map<String, String> getRawTagsMap() {
		return rawTagsMap;
	}

	@Override
	public TwitchMessage copy() {
		return new TwitchMessage(raw, origin, nickname, rawCommand, command, target, content, rawTags);
	}

	@Override
	public String toString() {
		return "TwitchMessage{" +
				"rawTagsMap=" + rawTagsMap +
				", badges=" + Arrays.toString(badges) +
				", color='" + color + '\'' +
				", displayName='" + displayName + '\'' +
				", userId='" + userId + '\'' +
				", type='" + type + '\'' +
				", mod=" + mod +
				", bits=" + bits +
				", language='" + language + '\'' +
				", isSub=" + isSub +
				", isTurbo=" + isTurbo +
				", messageId='" + messageId + '\'' +
				", roomId='" + roomId + '\'' +
				", " + super.toString().replace("Message{", "");
	}

	public static class Badge {
		private String type;
		private int version;

		public static Badge fromString(String str) {
			String[] badgeDetails = str.split("/");
			String type = Try.execute(() -> badgeDetails[0]).orElse(() -> "").getData();
			int version = Try.execute(() -> Integer.parseInt(badgeDetails[1])).orElse(() -> 0).getData();
			return new Badge(type, version);
		}

		public Badge(String type, int version) {
			this.type = type;
			this.version = version;
		}

		public String getType() {
			return type;
		}

		public int getVersion() {
			return version;
		}

		public void setType(String type) {
			this.type = type;
		}

		public void setVersion(int version) {
			this.version = version;
		}

		@Override
		public String toString() {
			return String.format("%s/%d", type, version);
		}
	}
}
