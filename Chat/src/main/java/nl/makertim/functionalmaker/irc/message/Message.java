package nl.makertim.functionalmaker.irc.message;

import nl.makertim.functionalmaker.irc.irc.Command;

public class Message {
	protected String raw;
	protected String origin;
	protected String nickname;
	protected String rawCommand;
	protected Command command;
	protected String target;
	protected String content;

	public Message() {
	}

	public Message(String raw, String origin, String nickname, String rawCommand, Command command, String target, String content) {
		this.raw = raw;
		this.origin = origin;
		this.nickname = nickname;
		this.rawCommand = rawCommand;
		this.command = command;
		this.target = target;
		this.content = content;
	}

	public void setRaw(String raw) {
		this.raw = raw;
	}

	public String getRaw() {
		return raw;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getRawCommand() {
		return rawCommand;
	}

	public Command getCommand() {
		return command;
	}

	public void setRawCommand(String rawCommand) {
		this.rawCommand = rawCommand;
		this.command = Command.getByCode(rawCommand);
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Message copy() {
		return new Message(raw, origin, nickname, rawCommand, command, target, content);
	}

	@Override
	public String toString() {
		return "Message{" +
				"origin='" + origin + '\'' +
				", nickname='" + nickname + '\'' +
				", command='" + command + '\'' +
				", rawCommand='" + rawCommand + '\'' +
				", target='" + target + '\'' +
				", content='" + content + '\'' +
				'}';
	}
}
