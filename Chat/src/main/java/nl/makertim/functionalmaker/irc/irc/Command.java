package nl.makertim.functionalmaker.irc.irc;

import nl.makertim.functionalmaker.Try;

import java.util.Arrays;

public enum Command {
	UNKNOWN,
	PRIVMSG,
	ABORT,
	ADMIN,
	ALIAS,
	ASSIGN,
	AWAY,
	BASICS,
	BEEP,
	BIND,
	BYE,
	CD,
	CLEARCHAT,
	CHANNEL,
	CLEAR,
	COMMANDS,
	COMMENT,
	CONNECT,
	CTCP,
	DATE,
	DCC,
	DEOP,
	DESCRIBE,
	DIE,
	DIGRAPH,
	DMSG,
	DQUERY,
	ECHO,
	ENCRYPT,
	ETIQUETTE,
	EVAL,
	EXEC,
	EXIT,
	EXPRESSIONS,
	FLUSH,
	FOREACH,
	GLOBALUSERSTATE,
	GMON_OUT,
	HELP,
	HISTORY,
	HOOK,
	IF,
	IGNORE,
	INFO,
	INPUT,
	INTRO,
	INVITE,
	IRCII,
	JOIN,
	KICK,
	KILL,
	LASTLOG,
	LEAVE,
	LINKS,
	LIST,
	LOAD,
	LUSERS,
	ME,
	MENUS,
	MLOAD,
	MODE,
	MOTD,
	MSG,
	NAMES,
	NEWS,
	NEWUSER,
	NICK,
	NOTE,
	NOTICE,
	NOTIFY,
	ON,
	OPER,
	PARSEKEY,
	PART,
	PING,
	QUERY,
	QUIT,
	QUOTE,
	RBIND,
	README,
	REDIRECT,
	REHASH,
	RESTART,
	ROOMSTATE,
	RULES,
	SAVE,
	SAY,
	SEND,
	SENDLINE,
	SERVER,
	SERVICES,
	SET,
	SIGNOFF,
	SLEEP,
	SQUIT,
	STATS,
	SUMMON,
	TIME,
	TIMER,
	TOPIC,
	TRACE,
	TYPE,
	USERHOST,
	USERNOTICE,
	USERSTATE,
	USERS,
	VERSION,
	WAIT,
	WALLOPS,
	WHICH,
	WHILE,
	WHO,
	WHOIS,
	WHOWAS,
	WINDOW,
	WHISPER,
	XECHO,
	XTYPE,
	/**
	 * The first message sent after client registration. The text used varies widely
	 */
	RPL_WELCOME(1),
	/**
	 * Part of the post-registration greeting. Text varies widely
	 */
	RPL_YOURHOST(2),
	/**
	 * Part of the post-registration greeting. Text varies widely
	 */
	RPL_CREATED(3),
	/**
	 * Part of the post-registration greeting
	 */
	RPL_MYINFO(4),
	/**
	 * Sent by the server to a user to suggest an alternative server, sometimes used when the connection is refused because the server is already full. Also known as RPL_SLINE (AustHex), and RPL_REDIR Also see #010.
	 */
	RPL_BOUNCE(5),
	/**
	 * See RFC
	 */
	RPL_TRACELINK(200),
	/**
	 * See RFC
	 */
	RPL_TRACECONNECTING(201),
	/**
	 * See RFC
	 */
	RPL_TRACEHANDSHAKE(202),
	/**
	 * See RFC
	 */
	RPL_TRACEUNKNOWN(203),
	/**
	 * See RFC
	 */
	RPL_TRACEOPERATOR(204),
	/**
	 * See RFC
	 */
	RPL_TRACEUSER(205),
	/**
	 * See RFC
	 */
	RPL_TRACESERVER(206),
	/**
	 * See RFC
	 */
	RPL_TRACESERVICE(207),
	/**
	 * See RFC
	 */
	RPL_TRACENEWTYPE(208),
	/**
	 * See RFC
	 */
	RPL_TRACECLASS(209),
	RPL_TRACERECONNECT(210),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSLINKINFO(211),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSCOMMANDS(212),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSCLINE(213),
	/**
	 * Reply to STATS (See RFC), Also known as RPL_STATSOLDNLINE (ircu, Unreal)
	 */
	RPL_STATSNLINE(214),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSILINE(215),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSKLINE(216),
	RPL_STATSQLINE(217),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSYLINE(218),
	/**
	 * End of RPL_STATS* list.
	 */
	RPL_ENDOFSTATS(219),
	/**
	 * Information about a user's own modes. Some daemons have extended the mode command and certain modes take parameters (like channel modes).
	 */
	RPL_UMODEIS(221),
	RPL_SERVICEINFO(231),
	RPL_ENDOFSERVICES(232),
	RPL_SERVICE(233),
	/**
	 * A service entry in the service list
	 */
	RPL_SERVLIST(234),
	/**
	 * Termination of an RPL_SERVLIST list
	 */
	RPL_SERVLISTEND(235),
	RPL_STATSVLINE(240),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSLLINE(241),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSUPTIME(242),
	/**
	 * Reply to STATS (See RFC); The info field is an extension found in some IRC daemons, which returns info such as an e-mail address or the name/job of an operator
	 */
	RPL_STATSOLINE(243),
	/**
	 * Reply to STATS (See RFC)
	 */
	RPL_STATSHLINE(244),
	RPL_STATSPING(246),
	RPL_STATSBLINE(247),
	RPL_STATSDLINE(250),
	/**
	 * Reply to LUSERS command, other versions exist (eg. RFC2812); Text may vary.
	 */
	RPL_LUSERCLIENT(251),
	/**
	 * Reply to LUSERS command - Number of IRC operators online
	 */
	RPL_LUSEROP(252),
	/**
	 * Reply to LUSERS command - Number of unknown/unregistered connections
	 */
	RPL_LUSERUNKNOWN(253),
	/**
	 * Reply to LUSERS command - Number of channels formed
	 */
	RPL_LUSERCHANNELS(254),
	/**
	 * Reply to LUSERS command - Information about local connections; Text may vary.
	 */
	RPL_LUSERME(255),
	/**
	 * Start of an RPL_ADMIN* reply. In practise, the server parameter is often never given, and instead the info field contains the text 'Administrative info about <server>'. Newer daemons seem to follow the RFC and output the server's hostname in the 'server' parameter, but also output the server name in the text as per traditional daemons.
	 */
	RPL_ADMINME(256),
	/**
	 * Reply to ADMIN command (Location, first line)
	 */
	RPL_ADMINLOC1(257),
	/**
	 * Reply to ADMIN command (Location, second line)
	 */
	RPL_ADMINLOC2(258),
	/**
	 * Reply to ADMIN command (E-mail address of administrator)
	 */
	RPL_ADMINEMAIL(259),
	/**
	 * See RFC
	 */
	RPL_TRACELOG(261),
	/**
	 * Used to terminate a list of RPL_TRACE* replies
	 */
	RPL_TRACEEND(262),
	/**
	 * When a server drops a command without processing it, it MUST use this reply. Also known as RPL_LOAD_THROTTLED and RPL_LOAD2HI, I'm presuming they do the same thing.
	 */
	RPL_TRYAGAIN(263),
	RPL_LOCALUSERS(265),
	RPL_GLOBALUSERS(266),
	/**
	 * Dummy reply, supposedly only used for debugging/testing new features, however has appeared in production daemons.
	 */
	RPL_NONE(300),
	/**
	 * Used in reply to a command directed at a user who is marked as away
	 */
	RPL_AWAY(301),
	/**
	 * Reply used by USERHOST (see RFC)
	 */
	RPL_USERHOST(302),
	/**
	 * Reply to the ISON command (see RFC)
	 */
	RPL_ISON(303),
	/**
	 * Reply from AWAY when no longer marked as away
	 */
	RPL_UNAWAY(305),
	/**
	 * Reply from AWAY when marked away
	 */
	RPL_NOWAWAY(306),
	/**
	 * Reply to WHOIS - Information about the user
	 */
	RPL_WHOISUSER(311),
	/**
	 * Reply to WHOIS - What server they're on
	 */
	RPL_WHOISSERVER(312),
	/**
	 * Reply to WHOIS - User has IRC Operator privileges
	 */
	RPL_WHOISOPERATOR(313),
	/**
	 * Reply to WHOWAS - Information about the user
	 */
	RPL_WHOWASUSER(314),
	/**
	 * Used to terminate a list of RPL_WHOREPLY replies
	 */
	RPL_ENDOFWHO(315),
	RPL_WHOISCHANOP(316),
	/**
	 * Reply to WHOIS - Idle information
	 */
	RPL_WHOISIDLE(317),
	/**
	 * Reply to WHOIS - End of list
	 */
	RPL_ENDOFWHOIS(318),
	/**
	 * Reply to WHOIS - Channel list for user (See RFC)
	 */
	RPL_WHOISCHANNELS(319),
	/**
	 * Channel list - Header
	 */
	RPL_LISTSTART(321),
	/**
	 * Channel list - A channel
	 */
	RPL_LIST(322),
	/**
	 * Channel list - End of list
	 */
	RPL_LISTEND(323),
	RPL_CHANNELMODEIS(324),
	RPL_UNIQOPIS(325),
	/**
	 * Response to TOPIC when no topic is set
	 */
	RPL_NOTOPIC(331),
	/**
	 * Response to TOPIC with the set topic
	 */
	RPL_TOPIC(332),
	/**
	 * Returned by the server to indicate that the attempted INVITE message was successful and is being passed onto the end client. Note that RFC1459 documents the parameters in the reverse order. The format given here is the format used on production servers, and should be considered the standard reply above that given by RFC1459.
	 */
	RPL_INVITING(341),
	/**
	 * Returned by a server answering a SUMMON message to indicate that it is summoning that user
	 */
	RPL_SUMMONING(342),
	/**
	 * An invite mask for the invite mask list
	 */
	RPL_INVITELIST(346),
	/**
	 * Termination of an RPL_INVITELIST list
	 */
	RPL_ENDOFINVITELIST(347),
	/**
	 * An exception mask for the exception mask list. Also known as RPL_EXLIST (Unreal, Ultimate)
	 */
	RPL_EXCEPTLIST(348),
	/**
	 * Termination of an RPL_EXCEPTLIST list. Also known as RPL_ENDOFEXLIST (Unreal, Ultimate)
	 */
	RPL_ENDOFEXCEPTLIST(349),
	/**
	 * Reply by the server showing its version details, however this format is not often adhered to
	 */
	RPL_VERSION(351),
	/**
	 * Reply to vanilla WHO (See RFC). This format can be very different if the 'WHOX' version of the command is used (see ircu).
	 */
	RPL_WHOREPLY(352),
	/**
	 * Reply to NAMES (See RFC)
	 */
	RPL_NAMREPLY(353),
	RPL_KILLDONE(361),
	RPL_CLOSING(362),
	RPL_CLOSEEND(363),
	/**
	 * Reply to the LINKS command
	 */
	RPL_LINKS(364),
	/**
	 * Termination of an RPL_LINKS list
	 */
	RPL_ENDOFLINKS(365),
	/**
	 * Termination of an RPL_NAMREPLY list
	 */
	RPL_ENDOFNAMES(366),
	/**
	 * A ban-list item (See RFC); <time left> and <reason> are additions used by KineIRCd
	 */
	RPL_BANLIST(367),
	/**
	 * Termination of an RPL_BANLIST list
	 */
	RPL_ENDOFBANLIST(368),
	/**
	 * Reply to WHOWAS - End of list
	 */
	RPL_ENDOFWHOWAS(369),
	/**
	 * Reply to INFO
	 */
	RPL_INFO(371),
	/**
	 * Reply to MOTD
	 */
	RPL_MOTD(372),
	RPL_INFOSTART(373),
	/**
	 * Termination of an RPL_INFO list
	 */
	RPL_ENDOFINFO(374),
	/**
	 * Start of an RPL_MOTD list
	 */
	RPL_MOTDSTART(375),
	/**
	 * Termination of an RPL_MOTD list
	 */
	RPL_ENDOFMOTD(376),
	/**
	 * Successful reply from OPER
	 */
	RPL_YOUREOPER(381),
	/**
	 * Successful reply from REHASH
	 */
	RPL_REHASHING(382),
	/**
	 * Sent upon successful registration of a service
	 */
	RPL_YOURESERVICE(383),
	RPL_MYPORTIS(384),
	/**
	 * Response to the TIME command. The string format may vary greatly. Also see #679.
	 */
	RPL_TIME(391),
	/**
	 * Start of an RPL_USERS list
	 */
	RPL_USERSSTART(392),
	/**
	 * Response to the USERS command (See RFC)
	 */
	RPL_USERS(393),
	/**
	 * Termination of an RPL_USERS list
	 */
	RPL_ENDOFUSERS(394),
	/**
	 * Reply to USERS when nobody is logged in
	 */
	RPL_NOUSERS(395),
	/**
	 * Used to indicate the nickname parameter supplied to a command is currently unused
	 */
	ERR_NOSUCHNICK(401),
	/**
	 * Used to indicate the server name given currently doesn't exist
	 */
	ERR_NOSUCHSERVER(402),
	/**
	 * Used to indicate the given channel name is invalid, or does not exist
	 */
	ERR_NOSUCHCHANNEL(403),
	/**
	 * Sent to a user who does not have the rights to send a message to a channel
	 */
	ERR_CANNOTSENDTOCHAN(404),
	/**
	 * Sent to a user when they have joined the maximum number of allowed channels and they tried to join another channel
	 */
	ERR_TOOMANYCHANNELS(405),
	/**
	 * Returned by WHOWAS to indicate there was no history information for a given nickname
	 */
	ERR_WASNOSUCHNICK(406),
	/**
	 * The given target(s) for a command are ambiguous in that they relate to too many targets
	 */
	ERR_TOOMANYTARGETS(407),
	/**
	 * Returned to a client which is attempting to send an SQUERY (or other message) to a service which does not exist
	 */
	ERR_NOSUCHSERVICE(408),
	/**
	 * PING or PONG message missing the originator parameter which is required since these commands must work without valid prefixes
	 */
	ERR_NOORIGIN(409),
	/**
	 * Returned when no recipient is given with a command
	 */
	ERR_NORECIPIENT(411),
	/**
	 * Returned when NOTICE/PRIVMSG is used with no message given
	 */
	ERR_NOTEXTTOSEND(412),
	/**
	 * Used when a message is being sent to a mask without being limited to a top-level domain (i.e. * instead of *.au)
	 */
	ERR_NOTOPLEVEL(413),
	/**
	 * Used when a message is being sent to a mask with a wild-card for a top level domain (i.e. *.*)
	 */
	ERR_WILDTOPLEVEL(414),
	/**
	 * Used when a message is being sent to a mask with an invalid syntax
	 */
	ERR_BADMASK(415),
	/**
	 * Returned when the given command is unknown to the server (or hidden because of lack of access rights)
	 */
	ERR_UNKNOWNCOMMAND(421),
	/**
	 * Sent when there is no MOTD to send the client
	 */
	ERR_NOMOTD(422),
	/**
	 * Returned by a server in response to an ADMIN request when no information is available. RFC1459 mentions this in the list of numerics. While it's not listed as a valid reply in section 4.3.7 ('Admin command'), it's confirmed to exist in the real world.
	 */
	ERR_NOADMININFO(423),
	/**
	 * Generic error message used to report a failed file operation during the processing of a command
	 */
	ERR_FILEERROR(424),
	/**
	 * Returned when a nickname parameter expected for a command isn't found
	 */
	ERR_NONICKNAMEGIVEN(431),
	/**
	 * Returned after receiving a NICK message which contains a nickname which is considered invalid, such as it's reserved ('anonymous') or contains characters considered invalid for nicknames. This numeric is misspelt, but remains with this name for historical reasons :)
	 */
	ERR_ERRONEUSNICKNAME(432),
	/**
	 * Returned by the NICK command when the given nickname is already in use
	 */
	ERR_NICKNAMEINUSE(433),
	/**
	 * Returned by a server to a client when it detects a nickname collision
	 */
	ERR_NICKCOLLISION(436),
	/**
	 * Return when the target is unable to be reached temporarily, eg. a delay mechanism in play, or a service being offline
	 */
	ERR_UNAVAILRESOURCE(437),
	/**
	 * Returned by the server to indicate that the target user of the command is not on the given channel
	 */
	ERR_USERNOTINCHANNEL(441),
	/**
	 * Returned by the server whenever a client tries to perform a channel effecting command for which the client is not a member
	 */
	ERR_NOTONCHANNEL(442),
	/**
	 * Returned when a client tries to invite a user to a channel they're already on
	 */
	ERR_USERONCHANNEL(443),
	/**
	 * Returned by the SUMMON command if a given user was not logged in and could not be summoned
	 */
	ERR_NOLOGIN(444),
	/**
	 * Returned by SUMMON when it has been disabled or not implemented
	 */
	ERR_SUMMONDISABLED(445),
	/**
	 * Returned by USERS when it has been disabled or not implemented
	 */
	ERR_USERSDISABLED(446),
	/**
	 * Returned by the server to indicate that the client must be registered before the server will allow it to be parsed in detail
	 */
	ERR_NOTREGISTERED(451),
	/**
	 * Returned by the server by any command which requires more parameters than the number of parameters given
	 */
	ERR_NEEDMOREPARAMS(461),
	/**
	 * Returned by the server to any link which attempts to register again
	 */
	ERR_ALREADYREGISTERED(462),
	/**
	 * Returned to a client which attempts to register with a server which has been configured to refuse connections from the client's host
	 */
	ERR_NOPERMFORHOST(463),
	/**
	 * Returned by the PASS command to indicate the given password was required and was either not given or was incorrect
	 */
	ERR_PASSWDMISMATCH(464),
	/**
	 * Returned to a client after an attempt to register on a server configured to ban connections from that client
	 */
	ERR_YOUREBANNEDCREEP(465),
	/**
	 * Sent by a server to a user to inform that access to the server will soon be denied
	 */
	ERR_YOUWILLBEBANNED(466),
	/**
	 * Returned when the channel key for a channel has already been set
	 */
	ERR_KEYSET(467),
	/**
	 * Returned when attempting to join a channel which is set +l and is already full
	 */
	ERR_CHANNELISFULL(471),
	/**
	 * Returned when a given mode is unknown
	 */
	ERR_UNKNOWNMODE(472),
	/**
	 * Returned when attempting to join a channel which is invite only without an invitation
	 */
	ERR_INVITEONLYCHAN(473),
	/**
	 * Returned when attempting to join a channel a user is banned from
	 */
	ERR_BANNEDFROMCHAN(474),
	/**
	 * Returned when attempting to join a key-locked channel either without a key or with the wrong key
	 */
	ERR_BADCHANNELKEY(475),
	/**
	 * The given channel mask was invalid
	 */
	ERR_BADCHANMASK(476),
	/**
	 * Returned when attempting to set a mode on a channel which does not support channel modes, or channel mode changes. Also known as ERR_MODELESS
	 */
	ERR_NOCHANMODES(477),
	/**
	 * Returned when a channel access list (i.e. ban list etc) is full and cannot be added to
	 */
	ERR_BANLISTFULL(478),
	/**
	 * Returned by any command requiring special privileges (eg. IRC operator) to indicate the operation was unsuccessful
	 */
	ERR_NOPRIVILEGES(481),
	/**
	 * Returned by any command requiring special channel privileges (eg. channel operator) to indicate the operation was unsuccessful
	 */
	ERR_CHANOPRIVSNEEDED(482),
	/**
	 * Returned by KILL to anyone who tries to kill a server
	 */
	ERR_CANTKILLSERVER(483),
	/**
	 * Sent by the server to a user upon connection to indicate the restricted nature of the connection (i.e. usermode +r)
	 */
	ERR_RESTRICTED(484),
	/**
	 * Any mode requiring 'channel creator' privileges returns this error if the client is attempting to use it while not a channel creator on the given channel
	 */
	ERR_UNIQOPRIVSNEEDED(485),
	/**
	 * Returned by OPER to a client who cannot become an IRC operator because the server has been configured to disallow the client's host
	 */
	ERR_NOOPERHOST(491),
	ERR_NOSERVICEHOST(492),
	/**
	 * Returned by the server to indicate that a MODE message was sent with a nickname parameter and that the mode flag sent was not recognised
	 */
	ERR_UMODEUNKNOWNFLAG(501),
	/**
	 * Error sent to any user trying to view or change the user mode for a user other than themselves
	 */
	ERR_USERSDONTMATCH(502),
	;
	private int code;

	Command() {
		this.code = Integer.MIN_VALUE;
	}

	Command(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	@Override
	public String toString() {
		if (getCode() == Integer.MIN_VALUE) {
			return name() + "()";
		}
		return name() + "(" + getCode() + ")";
	}

	public static Command getByCode(String code) {
		int i = Try.execute(() -> Integer.parseInt(code)).getData();
		return Arrays.stream(values())
				.filter(command -> command.code == i || command.name().equalsIgnoreCase(code))
				.findFirst()
				.orElse(UNKNOWN);
	}
}
