package nl.makertim.functionalmaker.irc.irc;

import nl.makertim.functionalmaker.irc.message.BasicMessageParser;
import nl.makertim.functionalmaker.irc.message.Message;

import java.net.Socket;
import java.util.function.Consumer;

public class IRCVanilla extends IRC<IRCVanilla, Message> {

	public IRCVanilla(Socket socket, Consumer<Throwable> onError) {
		super(socket, new BasicMessageParser(), onError);
	}

	@Override
	protected void registerDefaultListeners() {
		super.registerDefaultListeners();
		on(Command.RPL_WELCOME, message -> {
			onConnect();
		});
	}

	@Override
	public IRCVanilla login(String username, String nickname, String auth) {
		sendRaw(String.format("NICK %s", nickname));
		sendRaw(String.format("USER %s null null :%s", username, nickname));
		return returnThis();
	}

	@Override
	protected IRCVanilla returnThis() {
		return this;
	}
}
