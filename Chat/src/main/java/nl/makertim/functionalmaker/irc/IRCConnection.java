package nl.makertim.functionalmaker.irc;

import nl.makertim.functionalmaker.Try;
import nl.makertim.functionalmaker.irc.irc.IRC;
import nl.makertim.functionalmaker.reflection.ConstructorHelper;

import java.net.Socket;
import java.util.Optional;
import java.util.function.Consumer;

public class IRCConnection {

	private IRCConnection() {
	}

	public static <I extends IRC> Optional<I> connectIRC(Class<I> ircType, String serverAddress, int port, Consumer<Throwable> onError) {
		return Try.execute(() ->
				ConstructorHelper.createInstance(
						ircType, onError, new Socket(serverAddress, port), onError))
				.onFail(onError)
				.getData();
	}
}
