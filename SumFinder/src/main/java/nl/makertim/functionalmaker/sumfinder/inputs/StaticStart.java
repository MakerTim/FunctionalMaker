package nl.makertim.functionalmaker.sumfinder.inputs;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;

public class StaticStart implements Input {

	@Override
	public double getValue(RepetitionContext context) {
		return context.repetition.getStart();
	}

	@Override
	public String toString() {
		return "Start";
	}
}
