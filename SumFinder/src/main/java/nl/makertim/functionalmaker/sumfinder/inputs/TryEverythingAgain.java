package nl.makertim.functionalmaker.sumfinder.inputs;

import nl.makertim.functionalmaker.sumfinder.FunctionType;
import nl.makertim.functionalmaker.sumfinder.InputType;
import nl.makertim.functionalmaker.sumfinder.RepetitionContext;
import nl.makertim.functionalmaker.sumfinder.functions.Function;
import nl.makertim.functionalmaker.sumfinder.inputs.StaticNumbers.StaticNumber;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class TryEverythingAgain extends VariableInput {

	private static Input[] allFunctionsAgain() {
		List<Input> newInputs = Collections.synchronizedList(new ArrayList<>());
		Arrays.stream(InputType.values())
				.map(InputType::getInput)
				.filter(Objects::nonNull)
				.forEach(inputRaw -> {
					inputRaw.handle(input -> {
						Arrays.stream(FunctionType.values())
								.map(FunctionType::getFunction)
								.forEach(functionRaw -> {
									functionRaw.handle(function -> {
										for (int i = 2; i <= 4; i++) {
											newInputs.add(new ModifiedInput(input, function, i, true));
											newInputs.add(new ModifiedInput(input, function, i, false));
										}
									});
								});
					});
				});
		return newInputs.toArray(new Input[0]);
	}

	public TryEverythingAgain() {
		super(allFunctionsAgain());
	}

	@Override
	public void handle(Consumer<Input> handled) {
		Stream<Input> inputStream = Arrays.stream(inputs);
		if (Thread.currentThread().getName().startsWith("main")) {
			inputStream = inputStream.parallel();
		}
		inputStream.forEach(input -> {
			doSubJob(handled, input);
		});
	}

	private void doSubJob(Consumer<Input> handled, Input input) {
		handled.accept(input);
	}

	@Override
	public String getCollectionName() {
		return "Recursion";
	}

	private static class ModifiedInput implements Input {

		private final Input parent;
		private final Function function;
		private final double modifier;
		private final boolean isReversed;

		public ModifiedInput(Input parent, Function function, double modifier, boolean isReversed) {
			this.parent = parent;
			this.function = function;
			this.modifier = modifier;
			this.isReversed = isReversed;
		}

		@Override
		public double getValue(RepetitionContext context) {
			Input left = parent;
			Input right = new StaticNumber(modifier);
			if (isReversed) {
				left = right;
				right = parent;
			}
			return function.applyAsDouble(left, right, context);
		}

		@Override
		public String toString() {
			return String.format("(%s %s %s)", isReversed ? parent : modifier, function, isReversed ? modifier : parent);
		}
	}
}
