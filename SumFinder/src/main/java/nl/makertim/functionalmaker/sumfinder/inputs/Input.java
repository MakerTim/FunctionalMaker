package nl.makertim.functionalmaker.sumfinder.inputs;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;

import java.util.function.Consumer;

public interface Input {

	default void handle(Consumer<Input> handled) {
		handled.accept(this);
	}

	double getValue(RepetitionContext context);

}
