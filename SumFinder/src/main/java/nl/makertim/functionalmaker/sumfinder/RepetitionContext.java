package nl.makertim.functionalmaker.sumfinder;

public class RepetitionContext {

	private int n;
	public final Repetition repetition;

	public RepetitionContext(int n, Repetition repetition) {
		this.n = n;
		this.repetition = repetition;
	}

	public int getN() {
		return n;
	}

	public void addN(int n) {
		this.n += n;
	}

	public void addOneN() {
		this.n++;
	}

	public void setN(int n) {
		this.n = n;
	}
}
