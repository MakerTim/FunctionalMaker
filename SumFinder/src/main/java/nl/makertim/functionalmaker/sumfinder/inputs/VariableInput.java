package nl.makertim.functionalmaker.sumfinder.inputs;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;

import java.util.function.Consumer;

public abstract class VariableInput implements Input {

	protected final Input[] inputs;

	public VariableInput(Input[] inputs) {
		this.inputs = inputs;
	}

	public int variations() {
		return inputs.length;
	}

	public Input getInput(int i) {
		return inputs[i];
	}

	@Override
	public void handle(Consumer<Input> handled) {
		for (int i = 0; i < variations(); i++) {
			handled.accept(getInput(i));
		}
	}

	@Override
	public double getValue(RepetitionContext context) {
		throw new UnsupportedOperationException();
	}

	public abstract String getCollectionName();

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getCollectionName());
		builder.append("[");
		for (Input input : inputs) {
			builder.append(input.toString());
			builder.append(", ");
		}
		return builder.substring(0, builder.length() - 2) + "]";
	}
}
