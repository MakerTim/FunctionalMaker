package nl.makertim.functionalmaker.sumfinder.functions;

import nl.makertim.functionalmaker.functionalinterface.ToDoubleTriFunction;
import nl.makertim.functionalmaker.sumfinder.RepetitionContext;
import nl.makertim.functionalmaker.sumfinder.inputs.Input;

import java.util.function.Consumer;

public interface Function extends ToDoubleTriFunction<Input, Input, RepetitionContext> {

	default void handle(Consumer<Function> handled) {
		handled.accept(this);
	}
}
