package nl.makertim.functionalmaker.sumfinder;

import nl.makertim.functionalmaker.functionalinterface.ToDoubleTriFunction;
import nl.makertim.functionalmaker.sumfinder.inputs.Input;

public class RepetitionSet implements Input {

	private final Input inputLeft;
	private final ToDoubleTriFunction<Input, Input, RepetitionContext> function;
	private final Input inputRight;

	public RepetitionSet(Input inputLeft, ToDoubleTriFunction<Input, Input, RepetitionContext> function, Input inputRight) {
		this.inputLeft = inputLeft;
		this.function = function;
		this.inputRight = inputRight;
	}

	public Input getInputLeft() {
		return inputLeft;
	}

	public ToDoubleTriFunction<Input, Input, RepetitionContext> getFunction() {
		return function;
	}

	public Input getInputRight() {
		return inputRight;
	}

	@Override
	public double getValue(RepetitionContext context) {
		return getFunction().applyAsDouble(inputLeft, inputRight, context);
	}

	@Override
	public String toString() {
		return String.format("RepetitionSet<%s %s %s>", inputLeft, function, inputRight);
	}
}
