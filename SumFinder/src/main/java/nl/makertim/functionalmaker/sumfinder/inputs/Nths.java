package nl.makertim.functionalmaker.sumfinder.inputs;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Nths extends VariableInput {

	private static Nth[] inputsBetween(int min, int max, int step) {
		List<Nth> input = new ArrayList<>();
		for (int i = min; i <= max; i += step) {
			input.add(new Nth(i));
		}
		return input.stream()
				.sorted(Comparator.comparingInt(nth -> Math.abs(nth.i)))
				.toArray(Nth[]::new);
	}

	public Nths(int min, int max, int step) {
		super(inputsBetween(min, max, step));
	}

	@Override
	public String getCollectionName() {
		return "Nths";
	}

	private static class Nth implements Input {
		private int i;

		public Nth(int i) {
			this.i = i;
		}

		@Override
		public double getValue(RepetitionContext context) {
			return context.repetition.getNth(context.getN() + i);
		}

		@Override
		public String toString() {
			return "Nth" + i;
		}
	}
}
