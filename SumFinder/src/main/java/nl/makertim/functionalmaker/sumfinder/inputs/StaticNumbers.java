package nl.makertim.functionalmaker.sumfinder.inputs;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StaticNumbers extends VariableInput {

	private static StaticNumber[] inputsBetween(double min, double max, double step) {
		List<StaticNumber> input = new ArrayList<>();
		for (double d = max; d >= min; d -= step) {
			input.add(new StaticNumber(d));
		}
		return input.stream()
				.sorted(Comparator.comparingDouble(sn -> Math.abs(sn.d)))
				.toArray(StaticNumber[]::new);
	}

	@Override
	public String getCollectionName() {
		return "Static Numbers";
	}

	public StaticNumbers(double min, double max, double step) {
		super(inputsBetween(min, max, step));
	}

	static class StaticNumber implements Input {
		private final double d;

		public StaticNumber(double d) {
			this.d = d;
		}

		@Override
		public double getValue(RepetitionContext context) {
			return d;
		}

		@Override
		public String toString() {
			return Double.toString(d);
		}
	}
}
