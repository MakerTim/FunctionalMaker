package nl.makertim.sql;

import nl.makertim.functionalmaker.Try;
import nl.makertim.sql.mysql.MySQLConnection;
import org.junit.Before;

public class MySQLTest extends SQLTest {

	@Before
	public void setupConnection() {
		if (connection != null) return;
		Try.execute(() -> {
			connection = new MySQLConnection("localhost", 3307, "root", "12345", "temp?serverTimezone=UTC");
			connection.neverDisconnect();
			hasSql = connection.openConnection();
		}).onFail(Throwable::printStackTrace);
		if (!hasSql) {
			System.out.println("Skipping MYSQL");
		}
	}
}
