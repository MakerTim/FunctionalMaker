package nl.makertim.sql;

import nl.makertim.functionalmaker.Try;
import nl.makertim.sql.postgresql.PostgreSQLConnection;
import org.junit.Before;

public class PostgreSQLTest extends SQLTest {

	@Before
	public void setupConnection() {
		if (connection != null) return;
		Try.execute(() -> {
			connection = new PostgreSQLConnection("localhost", 5432, "postgres", "12345", "");
			connection.neverDisconnect();
			hasSql = connection.openConnection();
		}).onFail(Throwable::printStackTrace);
		if (!hasSql) {
			System.out.println("Skipping PostgreSQL");
		}
	}
}
