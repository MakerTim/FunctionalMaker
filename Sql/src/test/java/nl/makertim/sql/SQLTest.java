package nl.makertim.sql;

import nl.makertim.functionalmaker.AssertTry;
import nl.makertim.functionalmaker.Try;
import nl.makertim.sql.core.Connection;
import nl.makertim.sql.core.command.*;
import nl.makertim.sql.core.command.InsertCommand.ColumnValue;
import nl.makertim.sql.core.order.ConcreteOrder;
import nl.makertim.sql.core.order.Order;
import nl.makertim.sql.core.order.OrderType;
import nl.makertim.sql.core.types.*;
import nl.makertim.sql.core.types.Double;
import nl.makertim.sql.core.where.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.function.Consumer;

public class SQLTest {

	protected Connection connection;
	protected boolean hasSql = false;
	protected String mainTable = "test";
	protected String offTable = "testingcase";

	@BeforeClass
	public static void mention() {
		System.out.println();
		System.out.println("SQL TESTING");
		System.out.println();
	}

	@Test
	public void testStructure() throws Exception {
		if (!hasSql) return;

		StructureCommand structureCommand = new StructureCommand(mainTable);
		Column[] columns = connection.reverse(structureCommand);

		Assert.assertEquals(Type.TIMESTAMP, columns[0].type());
		Assert.assertEquals(Type.INTEGER, columns[1].type());
		Assert.assertEquals(Type.VARCHAR, columns[2].type());
		Assert.assertEquals("50", columns[2].getLength());
		AssertTry.any(
				() -> Assert.assertEquals(Type.TIMESTAMP, columns[3].type()),
				() -> Assert.assertEquals(Type.DATETIME, columns[3].type()));
		Assert.assertEquals(Type.CHAR, columns[4].type());
		Assert.assertEquals("2", columns[4].getLength());
	}

	@After
	public void closeConnection() {
		if (connection == null) return;
		connection.closeConnection();
	}


	@Test
	public void version() {
		if (!hasSql) return;
		String driver = connection.getDriverVersion();
		String server = connection.getServerVersion();

		Assert.assertNotNull(driver);
		Assert.assertNotNull(server);

		System.out.println("driver: " + driver);
		System.out.println("server: " + server);

		Assert.assertFalse(driver.isEmpty());
		Assert.assertFalse(server.isEmpty());
	}

	@Test
	public void connectTest() {
		if (!hasSql) return;
		System.out.println("Testing online!");
	}

	@SuppressWarnings("deprecation")
	private void testMakerTim(ResultSet rs) throws SQLException {
		if (rs.next()) {
			Date date = rs.getTimestamp("id");
			date.setHours(18);
			int number = rs.getInt("int");
			String name = rs.getString("string");
			Date other = rs.getTimestamp("other");
			other.setHours(8);

			Assert.assertEquals("2018-10-23 18:09:59.0", date.toString());
			Assert.assertEquals(2304, number);
			Assert.assertEquals("MakerTim", name);
			Assert.assertEquals("1996-04-23 08:06:00.0", other.toString());
		} else {
			throw new IndexOutOfBoundsException("No records in select");
		}
	}

	@SuppressWarnings("deprecation")
	private void testNietmij(ResultSet rs) throws SQLException {
		if (rs.next()) {
			Date date = rs.getTimestamp("id");
			date.setHours(18);
			int number = rs.getInt("int");
			String name = rs.getString("string");
			Date other = rs.getTimestamp("other");
			other.setHours(18);

			Assert.assertEquals("2018-10-23 18:34:07.0", date.toString());
			Assert.assertEquals(1212, number);
			Assert.assertEquals("nietmij", name);
			Assert.assertEquals("2018-10-23 18:34:05.0", other.toString());
		} else {
			throw new IndexOutOfBoundsException("No records in select");
		}
	}


	@Test
	@SuppressWarnings("deprecation")
	public void raw() throws SQLException {
		if (!hasSql) return;

		nl.makertim.sql.core.command.RawCommand raw = new nl.makertim.sql.core.command.RawCommand("SELECT * FROM test ORDER BY id LIMIT 1");
		Assert.assertEquals(nl.makertim.sql.core.command.RawCommand.class, raw.getType().getClassOfType());
		Assert.assertNull(raw.getTable());
		ResultSet rs = connection.execute(raw);

		testMakerTim(rs);
	}

	@Test
	public void select() throws SQLException {
		if (!hasSql) return;

		SelectCommand select = new SelectCommand(mainTable);
		Assert.assertEquals(SelectCommand.class, select.getType().getClassOfType());
		ResultSet rs = connection.execute(select);

		AssertTry.any(() -> {
			testMakerTim(rs);
		}, () -> {
			testNietmij(rs);
		}, () -> {
			Assert.assertNotNull(rs);
		});
	}

	@Test
	public void selectSpecific() throws SQLException {
		if (!hasSql) return;

		SelectCommand select = new SelectCommand(mainTable, new String[]{"id"});
		ResultSet rs = connection.execute(select);
		if (rs.next()) {
			AssertTry.assumeSafe(() -> rs.getTimestamp("id"));
			AssertTry.assumeThrows(() -> rs.getInt("int"), SQLException.class);
			AssertTry.assumeThrows(() -> rs.getString("string"), SQLException.class);
			AssertTry.assumeThrows(() -> rs.getTimestamp("other"), SQLException.class);
		} else {
			throw new IndexOutOfBoundsException("No records in select");
		}
	}

	@Test
	public void selectSpecific2() throws SQLException {
		if (!hasSql) return;

		SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "other"});
		ResultSet rs = connection.execute(select);
		if (rs.next()) {
			AssertTry.assumeSafe(() -> rs.getTimestamp("id"));
			AssertTry.assumeSafe(() -> rs.getInt("int"));
			AssertTry.assumeThrows(() -> rs.getString("string"), SQLException.class);
			AssertTry.assumeSafe(() -> rs.getTimestamp("other"));
		} else {
			throw new IndexOutOfBoundsException("No records in select");
		}
	}

	@Test
	public void selectWhere() throws SQLException {
		if (!hasSql) return;

		Where smallerThen2304 = new WhereSmaller("int", 2304);
		SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "string", "other"}, smallerThen2304);
		ResultSet rs = connection.execute(select);

		testNietmij(rs);
	}

	@Test
	public void selectDistinctWhere() throws SQLException {
		if (!hasSql) return;

		Where smallerThen2304 = new WhereSmaller("int", 2304);
		SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "string", "other"}, smallerThen2304);
		select.hasDupes(false);
		ResultSet rs = connection.execute(select);

		testNietmij(rs);
	}

	@Test
	public void selectWhereNoOrder() throws SQLException {
		if (!hasSql) return;

		Where biggerThen0 = new WhereBigger("int", 0);
		Order order = new Order();
		SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "string", "other"}, biggerThen0, order);
		ResultSet rs = connection.execute(select);

		AssertTry.assumeSafe(rs::next);
		AssertTry.assumeSafe(rs::next);
		AssertTry.assumeSafe(rs::next);
	}

	@Test
	public void selectWhereOrder() throws SQLException {
		if (hasSql) {
			Where biggerThen0 = new WhereBigger("int", 0);
			Order order = new Order(new ConcreteOrder("id", OrderType.ASCENDING));
			SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "string", "other"}, biggerThen0, order);
			ResultSet rs = connection.execute(select);

			testMakerTim(rs);
			testNietmij(rs);
		}
		if (hasSql) {
			Where biggerThen0 = new WhereBigger("int", 0);
			Order order = new Order(new ConcreteOrder("id", OrderType.DESCENDING));
			SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "string", "other"}, biggerThen0, order);
			ResultSet rs = connection.execute(select);

			testNietmij(rs);
			testMakerTim(rs);
		}
	}

	@Test
	public void selectWhereOrderCount() throws SQLException {
		if (hasSql) {
			Where biggerThen0 = new WhereBigger("int", 0);
			Order order = new Order("id", OrderType.ASCENDING);
			SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "string", "other"}, biggerThen0, order, 1);
			ResultSet rs = connection.execute(select);

			testMakerTim(rs);
			Assert.assertFalse(rs.next());
		}
		if (hasSql) {
			Where biggerThen0 = new WhereBigger("int", 0);
			Order order = new Order(new ConcreteOrder("id", OrderType.DESCENDING));
			order.addOrder(new ConcreteOrder("int", OrderType.ASCENDING));
			SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "string", "other"}, biggerThen0, order, 1);
			ResultSet rs = connection.execute(select);

			testNietmij(rs);
			Assert.assertFalse(rs.next());
		}
	}

	@Test
	public void selectWhereOrderStartCount() throws SQLException {
		if (!hasSql) {
			return;
		}
		Where biggerThen0 = new WhereBigger("int", 0);
		Order order = new Order(new ConcreteOrder("int", OrderType.ASCENDING));
		SelectCommand select = new SelectCommand(mainTable, new String[]{"id", "int", "string", "other"}, biggerThen0, order, 1);
		select.setStartAndRows(1, 1);
		ResultSet rs = connection.execute(select);

		testMakerTim(rs);
		Assert.assertFalse(rs.next());
	}

	@Test
	public void updateAll() throws SQLException {
		if (!hasSql) return;

		UpdateCommand update = new UpdateCommand(mainTable, "unused", Integer.toString(new Random().nextInt(80) + 10));
		ResultSet rs = connection.execute(update);

		AssertTry.assumeSafe(rs::next);
	}

	@Test
	public void updateAllLimited() throws SQLException {
		if (!hasSql) return;

		UpdateCommand update = new UpdateCommand(mainTable, "unused", Integer.toString(new Random().nextInt(80) + 10), 1);
		ResultSet rs = connection.execute(update);

		AssertTry.assumeSafe(rs::next);
	}

	@Test
	public void updateWhere() throws SQLException {
		if (!hasSql) return;

		Where nameIsUpdateable = new WhereEquals("string", "updateable");
		UpdateCommand update = new UpdateCommand(mainTable, "unused", Integer.toString(new Random().nextInt(80) + 10), nameIsUpdateable);
		ResultSet rs = connection.execute(update);

		AssertTry.assumeSafe(rs::next);
	}

	@Test
	public void updateWhereLimited() throws SQLException {
		if (!hasSql) return;

		Where biggerThen0 = new WhereBigger("string", "0");
		UpdateCommand update = new UpdateCommand(mainTable, "unused", Integer.toString(new Random().nextInt(80) + 10), biggerThen0, 0);
		update.setRows(1);
		ResultSet rs = connection.execute(update);

		AssertTry.assumeSafe(rs::next);
	}

	@Test
	public void updateWhereOrder() throws SQLException {
		if (!hasSql) return;

		Where biggerThen0 = new WhereBigger("string", "0");
		Order order = new Order("unused", OrderType.DESCENDING);
		UpdateCommand update = new UpdateCommand(mainTable, "unused", Integer.toString(new Random().nextInt(80) + 10), biggerThen0, order);
		update.setOrder(update.getOrder());
		ResultSet rs = connection.execute(update);

		AssertTry.assumeSafe(rs::next);
	}

	@Test
	public void updateWhereOrderLimited() throws SQLException {
		if (!hasSql) return;

		Where biggerThen0 = new WhereBigger("string", "0");
		Order order = new Order("unused", OrderType.ASCENDING);
		UpdateCommand update = new UpdateCommand(mainTable, "unused", Integer.toString(new Random().nextInt(80) + 10), biggerThen0, order, 1);
		ResultSet rs = connection.execute(update);

		AssertTry.assumeSafe(rs::next);
	}

	@Test
	public void alter() throws SQLException {
		if (!hasSql) return;

		{
			Column unusedColumn = new VarChar("unused", 3, null);
			AlterCommand alter = new AlterCommand(mainTable, unusedColumn);
			ResultSet rs = connection.execute(alter);

			AssertTry.assumeSafe(rs::next);
		}
		{
			Column unusedColumn = new Char("unused", 2, null);
			AlterCommand alter = new AlterCommand(mainTable, unusedColumn);
			ResultSet rs = connection.execute(alter);

			AssertTry.assumeSafe(rs::next);
		}

	}

	@Test
	public void createFailNoColumns() {
		if (!hasSql) return;

		CreateCommand create = new CreateCommand(offTable);

		Consumer<Throwable> onError = connection.getOnError();
		connection.setOnError(throwable -> {
		});
		AssertTry.assumeThrows(() -> connection.execute(create), SQLException.class);
		connection.setOnError(onError);
	}

	@Test
	public void createAndDrop() throws SQLException {
		if (!hasSql) return;

		// Because tests have no order: combine them here for order
		createIgnoreFail();
		drop();

		create();
		createFail();
		createIgnoreFail();

		DeleteCommand delete = new DeleteCommand(offTable);
		AssertTry.assumeSafe(() -> connection.execute(delete));

		drop();
		createMoreColumns();

		truncate();

		drop();
	}

	private void create() throws SQLException {
		Column unusedColumn = new VarChar("unused", 3, null);
		CreateCommand create = new CreateCommand(offTable, unusedColumn);
		ResultSet rs = connection.execute(create);

		Assert.assertNotNull(rs);
		AssertTry.assumeSafe(rs::next);
	}

	private void createFail() {
		Column unusedColumn = new VarChar("unused", 3, null);
		CreateCommand create = new CreateCommand(offTable, unusedColumn);

		Consumer<Throwable> onError = connection.getOnError();
		connection.setOnError(throwable -> {
		});
		AssertTry.assumeThrows(() -> connection.execute(create), SQLException.class);
		connection.setOnError(onError);
	}

	private void createIgnoreFail() throws SQLException {
		Column unusedColumn = new VarChar("unused", 3, null);
		CreateCommand create = new CreateCommand(offTable, unusedColumn);
		create.setIgnoreIfExists(true);
		ResultSet rs = connection.execute(create);

		Assert.assertFalse(rs.next()); // not great test
	}

	@SuppressWarnings("deprecation")
	private void createMoreColumns() throws SQLException {
		Column bigColumn = new BigInt("big", null);
		Column bitColumn = new Bit("bool", null);
		Column blobColumn = new Blob("binary", null);
		Column charColumn = new Char("abc", 1, null);
		Column dateColumn = new nl.makertim.sql.core.types.Date("today", null);
		Column dateTimeColumn = new DateTime("now", null);
		Column decimalColumn = new Decimal("money", null);
		Column doubleColumn = new Double("stats", null);
		Column intColumn = new Int("employee", null);
		Column longBlobColumn = new LongBlob("manybinary", null);
		Column textColumn = new Text("dear_diary", null);
		Column timeColumn = new Time("time", null);
		Column timestampColumn = new TimeStamp("this", null);
		Column tinyIntColumn = new TinyInt("penes", null);
		Column varcharColumn = new VarChar("uuid", 36, null);
		Column yearColumn = new Year("year", null);
		Column custom = new CustomColumn("enum", "0", "VARCHAR(1)", null);
		CreateCommand create = new CreateCommand(offTable);
		create.addColumn(bigColumn);
		create.addColumn(bitColumn);
		create.addColumn(blobColumn);
		create.addColumn(charColumn);
		create.addColumnAt(0, dateColumn);
		create.addColumnAfter(dateColumn.columnName(), dateTimeColumn);
		create.addColumnAfter(dateTimeColumn, decimalColumn);
		create.addColumnBefore(dateColumn, doubleColumn);
		create.addColumnBefore(dateColumn.columnName(), intColumn);
		create.addColumn(longBlobColumn);
		create.addColumn(textColumn);
		create.addColumn(timeColumn);
		create.addColumn(timestampColumn);
		create.addColumn(tinyIntColumn);
		create.addColumnAt(0, varcharColumn);
		create.addColumn(yearColumn);
		create.addColumnAfter("none-existing-column", custom);
		ResultSet rs = connection.execute(create);
		// Order:
		// uuid, stats, employee, today, now, money, big, bit, binary, abc, manybinary, dear_diary, time, this, penes, year, enum

		AssertTry.assumeSafe(() -> {
			if (rs != null) {
				rs.next();
			}
		});
	}

	private void drop() throws SQLException {
		DropCommand drop = new DropCommand(offTable);
		ResultSet rs = connection.execute(drop);

		Assert.assertFalse(rs.next()); // not great test
	}

	private void truncate() throws SQLException {
		TruncateCommand truncate = new TruncateCommand(offTable);
		ResultSet rs = connection.execute(truncate);

		Assert.assertFalse(rs.next()); // not great test
	}

	@Test
	public void insertAndDelete() throws SQLException {
		if (!hasSql) return;

		// Because tests have no order: combine them here for order
		String id = "2020-10-24 13:39:29";
		String name = "automated test";
		insert(id, name);
		delete(name);

		insert(id.replace("2020", "2021"), name);
		insert(id.replace("2020", "2022"), name);
		insert(id.replace("2020", "2023"), name);
		deleteLimit(name);
		deleteLimit(name);
		deleteLimit(name);
	}

	protected void insert(String idString, String name) throws SQLException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		ColumnValue id = new ColumnValue("id", Try.execute(() -> dateFormat.parse(idString)).getData());
		ColumnValue integer = new ColumnValue("int", -2);
		ColumnValue string = new ColumnValue("string", name);
		ColumnValue other = new ColumnValue("other", Try.execute(() -> dateFormat.parse("1996-04-23 13:37:42")).getData());
		ColumnValue unused = new ColumnValue("unused", 0);
		InsertCommand insert = new InsertCommand(mainTable, id, integer, string, other);
		insert.addValue(unused);
		ResultSet rs = connection.execute(insert);

		Assert.assertNotNull(rs);
		AssertTry.assumeSafe(rs::next);
	}

	private void delete(String name) {
		Where key = new WhereEquals("string", name);
		DeleteCommand delete = new DeleteCommand(mainTable, key);
		AssertTry.assumeSafe(() -> connection.execute(delete));
	}

	protected void deleteLimit(Object name) {
		Where key = new WhereLike("string", name);
		DeleteCommand delete = new DeleteCommand(mainTable, key);
		delete.setRows(1);
		AssertTry.assumeSafe(() -> connection.execute(delete));
	}

	@Test
	public void whereTests() {
		if (!hasSql) return;

		Where[] whereClauses = new Where[]{
				new CombineWhereAND(new WhereBiggerEquals("int", 0), new WhereSmallerEquals("int", 9000)),
				new CombineWhereNot(new WhereBigger("int", 0), new WhereSmaller("int", 0)),
				new CombineWhereXOR(new WhereLike("string", "makertim"), new WhereEquals("string", "makertim")),
				new CombineWhereOR(new WhereIn("string", "MakerTim", "nietmij", "updateable"), new WhereBetween("int", 0, 9000)),
				new WhereNot("int", 2304),
				new WhereNotBetween("int", 0, 9000),

				new CombineWhereAND(null, new WhereEquals("int", 9000)),
				new CombineWhereAND(new WhereEquals("int", 9000), null),
				new CombineWhereAND(null, null),
		};

		for (Where where : whereClauses) {
			SelectCommand select = new SelectCommand(mainTable, where);
			ResultSet rs = connection.execute(select);

			Assert.assertNotNull(where.getClass().toString() + "\n" + connection.toSql(select), rs);
		}

		WhereBetween between = new WhereBetween("int", "0", "2020");
		AssertTry.assumeThrows(between::getValue, IllegalArgumentException.class);

		WhereIn in = new WhereIn("int", "0", "2020");
		AssertTry.assumeThrows(in::getValue, IllegalArgumentException.class);
	}

	@Test
	public void close() {
		closeConnection();
	}
}
