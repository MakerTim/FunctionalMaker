package nl.makertim.sql.mysql;

import nl.makertim.functionalmaker.Try;
import nl.makertim.sql.core.Connection;
import nl.makertim.sql.core.translator.CommandTranslator;

import java.sql.ResultSet;

public class MySQLConnection extends Connection {

	public MySQLConnection(String server, int port, String username, String password, String database) {
		this("mysql", server, port, username, password, database);
	}

	protected MySQLConnection(String type, String server, int port, String username, String password, String database) {
		super(type, server, port, username, password, database);
	}

	@Override
	protected String getDriverClass() {
		return "com.mysql.cj.jdbc.MysqlDataSource";
	}

	@Override
	@SuppressWarnings("deprecation")
	public String getServerVersion() {
		ResultSet rs = selectQuery(new nl.makertim.sql.core.command.RawCommand("SELECT @@version as \"version\";"));
		return Try.execute(() ->
				(rs != null && rs.next()) ? rs.getString("version") : null
		).getData();
	}

	@Override
	protected CommandTranslator getNewTranslator() {
		return new MySQLTranslator();
	}
}
