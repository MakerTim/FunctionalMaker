package nl.makertim.sql.mysql;

import nl.makertim.sql.core.translator.*;

public class MySQLTranslator implements CommandTranslator, SQLWhereTranslator, SQLTypeTranslator {

	@Override
	public char getQuote() {
		return '`';
	}

	@Override
	public String escapeQuote() {
		return "``";
	}

	@Override
	public char stringQuote() {
		return '"';
	}

	@Override
	public String escapeStringQuote() {
		return "\"\"";
	}

	@Override
	public char separator() {
		return ',';
	}

	@Override
	public WhereTranslator getWhereTranslator() {
		return this;
	}

	@Override
	public TypeTranslator getTypeTranslator() {
		return this;
	}

	@Override
	public CommandTranslator getTranslator() {
		return this;
	}

	@Override
	public String getSelectFormat() {
		return "SELECT {{distinct}} {{columns}} FROM {{table}} {{where}} {{order}} {{limit}}";
	}

	@Override
	public String getUpdateFormat() {
		return "UPDATE {{table}} SET {{column-values}} {{where}} {{limit}}";
	}

	@Override
	public String getDeleteFormat() {
		return "DELETE FROM {{table}} {{where}} {{limit}}";
	}

	@Override
	public String getInsertFormat() {
		return "INSERT INTO {{table}} {{column}} VALUES {{values}}";
	}

	@Override
	public String getCreateFormat() {
		return "CREATE TABLE {{exists}} {{table}} ({{columns-typed}})";
	}

	@Override
	public String getAlterFormat() {
		return "ALTER TABLE {{table}} CHANGE COLUMN {{column}} {{column-typed}}";
	}

	@Override
	public String getDropFormat() {
		return "DROP TABLE {{table}}";
	}

	@Override
	public String getTruncateFormat() {
		return "TRUNCATE {{table}}";
	}

}
