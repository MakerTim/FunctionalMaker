package nl.makertim.sql.mariadb;

import nl.makertim.sql.core.translator.CommandTranslator;
import nl.makertim.sql.mysql.MySQLConnection;

public class MariaDBConnection extends MySQLConnection {

	public MariaDBConnection(String server, int port, String username, String password, String database) {
		this("mariadb", server, port, username, password, database);
	}

	protected MariaDBConnection(String type, String server, int port, String username, String password, String database) {
		super(type, server, port, username, password, database);
	}

	@Override
	protected CommandTranslator getNewTranslator() {
		return new MariaDBTranslator();
	}

	@Override
	protected String getDriverClass() {
		return "org.mariadb.jdbc.Driver";
	}
}
