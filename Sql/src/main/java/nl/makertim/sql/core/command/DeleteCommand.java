package nl.makertim.sql.core.command;

import nl.makertim.sql.core.where.Where;

import java.util.ArrayList;
import java.util.List;

public class DeleteCommand extends AbstractCommand<DeleteCommand>
		implements WhereCommand<DeleteCommand>, LimitedCommand<DeleteCommand> {

	private Where where;
	private List<Object> variables = new ArrayList<>();
	private int start = -1;
	private int rows = -1;

	public DeleteCommand(String table, Where where) {
		super(table);
		this.where = where;
	}

	public DeleteCommand(String table) {
		this(table, Where.NONE);
	}

	@Override
	public CommandType<DeleteCommand> getType() {
		return CommandType.DELETE;
	}

	@Override
	public Where getWhere() {
		return where;
	}

	@Override
	public void addParameter(Object value) {
		variables.add(value);
	}

	@Override
	public Object[] getParameters() {
		return variables.toArray(new Object[0]);
	}

	@Override
	public int getRows() {
		return rows;
	}

	@Override
	public void setRows(int rows) {
		this.rows = rows;
	}

}
