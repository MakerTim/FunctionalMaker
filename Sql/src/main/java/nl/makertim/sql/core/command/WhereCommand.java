package nl.makertim.sql.core.command;

import nl.makertim.sql.core.where.Where;

public interface WhereCommand<S extends Command<S>> extends ParameterCommand<S> {

	Where getWhere();
}
