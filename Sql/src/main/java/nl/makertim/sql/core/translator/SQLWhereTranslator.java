package nl.makertim.sql.core.translator;

import nl.makertim.sql.core.command.ParameterCommand;
import nl.makertim.sql.core.where.*;

import java.util.Arrays;

public interface SQLWhereTranslator extends WhereTranslator {

	CommandTranslator getTranslator();

	@Override
	default String combineAND(ParameterCommand<?> cmd, CombineWhereAND combineWhere) {
		return '(' + toSql(cmd, combineWhere.getLeft()) + ") AND (" + toSql(cmd, combineWhere.getRight()) + ')';
	}

	@Override
	default String combineOR(ParameterCommand<?> cmd, CombineWhereOR combineWhere) {
		return '(' + toSql(cmd, combineWhere.getLeft()) + ") OR (" + toSql(cmd, combineWhere.getRight()) + ')';
	}

	@Override
	default String combineXOR(ParameterCommand<?> cmd, CombineWhereXOR combineWhere) {
		return '(' + toSql(cmd, combineWhere.getLeft()) + ") XOR (" + toSql(cmd, combineWhere.getRight()) + ')';
	}

	@Override
	default String combineNot(ParameterCommand<?> cmd, CombineWhereNot combineWhere) {
		return '(' + toSql(cmd, combineWhere.getLeft()) + ") != (" + toSql(cmd, combineWhere.getRight()) + ')';
	}

	@Override
	default String toSmallerSql(ParameterCommand<?> cmd, WhereSmaller where) {
		cmd.addParameter(where.getValue());
		return getTranslator().safeName(where.getColumn()) + " < ?";
	}

	@Override
	default String toSmallerEqualsSql(ParameterCommand<?> cmd, WhereSmallerEquals where) {
		cmd.addParameter(where.getValue());
		return getTranslator().safeName(where.getColumn()) + " <= ?";
	}

	@Override
	default String toNotBetweenSql(ParameterCommand<?> cmd, WhereNotBetween where) {
		cmd.addParameter(where.getSmall());
		cmd.addParameter(where.getBig());
		return getTranslator().safeName(where.getColumn()) + " NOT BETWEEN ? AND ?";
	}

	@Override
	default String toLikeSql(ParameterCommand<?> cmd, WhereLike where) {
		cmd.addParameter(where.getValue());
		return getTranslator().safeName(where.getColumn()) + " LIKE ?";
	}

	@Override
	default String toInSql(ParameterCommand<?> cmd, WhereIn where) {
		String list =
				String.join(getTranslator().separator() + " ",
						Arrays.stream(where.getValues())
								.map(Object::toString)
								.map(getTranslator()::safeValue)
								.toArray(String[]::new));
		return getTranslator().safeName(where.getColumn()) + "IN (" + list + ") ";
	}

	@Override
	default String toBetweenSql(ParameterCommand<?> cmd, WhereBetween where) {
		cmd.addParameter(where.getSmall());
		cmd.addParameter(where.getBig());
		return getTranslator().safeName(where.getColumn()) + " BETWEEN ? AND ?";
	}

	@Override
	default String toNotSql(ParameterCommand<?> cmd, WhereNot where) {
		cmd.addParameter(where.getValue());
		return getTranslator().safeName(where.getColumn()) + " <> ?";
	}

	@Override
	default String toEqualsSql(ParameterCommand<?> cmd, WhereEquals where) {
		cmd.addParameter(where.getValue());
		return getTranslator().safeName(where.getColumn()) + " = ?";
	}

	@Override
	default String toBiggerSql(ParameterCommand<?> cmd, WhereBigger where) {
		cmd.addParameter(where.getValue());
		return getTranslator().safeName(where.getColumn()) + " > ?";
	}

	@Override
	default String toBiggerEqualsSql(ParameterCommand<?> cmd, WhereBiggerEquals where) {
		cmd.addParameter(where.getValue());
		return getTranslator().safeName(where.getColumn()) + " >= ?";
	}

}
