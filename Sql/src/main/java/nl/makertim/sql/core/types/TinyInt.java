package nl.makertim.sql.core.types;

public class TinyInt extends AbstractSimpleColumn {

	public TinyInt(String columnName, String defaultValue) {
		super(columnName, 3, Type.INTEGER, defaultValue);
	}

}
