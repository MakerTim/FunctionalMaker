package nl.makertim.sql.core.where;

public class CombineWhereNot extends CombineWhere {

	public CombineWhereNot(Where left, Where right) {
		super(left, CombineWhereType.NOT, right);
	}
}
