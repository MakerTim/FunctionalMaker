package nl.makertim.sql.core.types;

public class Year extends AbstractSimpleColumn {

	public Year(String columnName, String defaultValue) {
		super(columnName, 0, Type.YEAR, defaultValue);
	}

}
