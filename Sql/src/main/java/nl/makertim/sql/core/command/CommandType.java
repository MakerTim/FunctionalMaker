package nl.makertim.sql.core.command;

public final class CommandType<C extends Command> {

	public static final CommandType<SelectCommand> SELECT = new CommandType<>(InnerType.SELECT, SelectCommand.class);
	public static final CommandType<UpdateCommand> UPDATE = new CommandType<>(InnerType.UPDATE, UpdateCommand.class);
	public static final CommandType<DeleteCommand> DELETE = new CommandType<>(InnerType.DELETE, DeleteCommand.class);
	public static final CommandType<InsertCommand> INSERT = new CommandType<>(InnerType.INSERT, InsertCommand.class);
	public static final CommandType<CreateCommand> CREATE = new CommandType<>(InnerType.CREATE, CreateCommand.class);
	public static final CommandType<AlterCommand> ALTER = new CommandType<>(InnerType.ALTER, AlterCommand.class);
	public static final CommandType<DropCommand> DROP = new CommandType<>(InnerType.DROP, DropCommand.class);
	public static final CommandType<TruncateCommand> TRUNCATE = new CommandType<>(InnerType.TRUNCATE, TruncateCommand.class);
	public static final CommandType<StructureCommand> GET_STRUCTURE = new CommandType<>(InnerType.GET_STRUCTURE, StructureCommand.class);
	@SuppressWarnings("deprecation")
	public static final CommandType<RawCommand> UNKNOWN = new CommandType<>(InnerType.UNKNOWN, RawCommand.class);

	private InnerType innerType;
	private Class<C> typeClass;

	private CommandType(InnerType innerType, Class<C> typeClass) {
		this.innerType = innerType;
		this.typeClass = typeClass;
	}

	public Class<C> getClassOfType() {
		return typeClass;
	}

	public InnerType getInnerType() {
		return innerType;
	}

	public enum InnerType {
		SELECT,
		UPDATE,
		DELETE,
		INSERT,
		CREATE,
		ALTER,
		DROP,
		TRUNCATE,
		GET_STRUCTURE,
		UNKNOWN
	}
}
