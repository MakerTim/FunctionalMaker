package nl.makertim.sql.core.where;

public class WhereLike extends ConcreteWhere {

	public WhereLike(String column, Object value) {
		super(column, ConcreteWhereType.LIKE, value);
	}
}
