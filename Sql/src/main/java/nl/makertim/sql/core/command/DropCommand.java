package nl.makertim.sql.core.command;

public class DropCommand extends AbstractCommand<DropCommand> {

	public DropCommand(String table) {
		super(table);
	}

	@Override
	public CommandType<DropCommand> getType() {
		return CommandType.DROP;
	}
}
