package nl.makertim.sql.core.types;

public class Blob extends AbstractSimpleColumn {

	public Blob(String columnName, String defaultValue) {
		super(columnName, 0, Type.BLOB, defaultValue);
	}

	public void setLength(int length) {
		this.length = length;
	}

}
