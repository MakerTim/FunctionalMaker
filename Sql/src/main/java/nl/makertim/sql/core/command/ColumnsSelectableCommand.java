package nl.makertim.sql.core.command;

public interface ColumnsSelectableCommand<S extends Command<S>> {

	String[] getColumns();

}
