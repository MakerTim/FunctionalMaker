package nl.makertim.sql.core.command;

import nl.makertim.sql.core.types.Column;

public class AlterCommand extends AbstractCommand<AlterCommand> {

	protected String oldColumnName;
	protected Column newColumn;

	// column placement, after / before @See CreateCommand
	
	public AlterCommand(String table, Column newColumn) {
		this(table, newColumn.columnName(), newColumn);
	}

	public AlterCommand(String table, String oldColumnName, Column newColumn) {
		super(table);
		this.oldColumnName = oldColumnName;
		this.newColumn = newColumn;
	}

	@Override
	public CommandType<AlterCommand> getType() {
		return CommandType.ALTER;
	}

	public String getOldColumnName() {
		return oldColumnName;
	}

	public Column getNewColumn() {
		return newColumn;
	}
}
