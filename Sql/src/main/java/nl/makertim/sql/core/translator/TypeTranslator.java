package nl.makertim.sql.core.translator;

import nl.makertim.sql.core.types.*;

public interface TypeTranslator {

	default String toSql(Column column) {
		switch (column.type()) {
		case INTEGER:
			return integerType(column);
		case DECIMAL:
			return decimalType(column);
		case CHAR:
			return charType(column);
		case VARCHAR:
			return varCharType(column);
		case TEXT:
			return textType(column);
		case BLOB:
			if (column.getLength().equals("0")) {
				return blobType(column);
			} else {
				((Blob) column).setLength(0);
				return bigblobType(column);
			}
		case DATE:
			return dateType(column);
		case TIME:
			return timeType(column);
		case YEAR:
			return yearType(column);
		case DATETIME:
			return datetimeType(column);
		case TIMESTAMP:
			return timestampType(column);
		case CUSTOM:
		default:
			return customType((CustomColumn) column);
		}
	}

	String integerType(Column column);

	String decimalType(Column column);

	String charType(Column column);

	String varCharType(Column column);

	String textType(Column column);

	String blobType(Column column);

	String bigblobType(Column column);

	String dateType(Column column);

	String timeType(Column column);

	String yearType(Column column);

	String datetimeType(Column column);

	String timestampType(Column column);

	String customType(CustomColumn column);

	default Type reverse(String name) {
		Type type = Type.CUSTOM;
		name = name.toLowerCase();
		for (Type testedType : Type.values()) {
			if (testedType == Type.CUSTOM) {
				break;
			}
			Column column = new AbstractSimpleColumn("", 0, testedType, null) {
			};
			String translatedType = toSql(column).toLowerCase();
			if (translatedType.contains(name)) {
				type = testedType;
				break;
			}
		}
		return type;
	}
}
