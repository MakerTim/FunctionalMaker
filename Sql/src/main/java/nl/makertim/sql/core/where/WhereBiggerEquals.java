package nl.makertim.sql.core.where;

public class WhereBiggerEquals extends ConcreteWhere {

	public WhereBiggerEquals(String column, Object value) {
		super(column, ConcreteWhereType.BIGGER_EQUALS, value);
	}
}
