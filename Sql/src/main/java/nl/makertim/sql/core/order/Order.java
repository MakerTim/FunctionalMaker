package nl.makertim.sql.core.order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Order {

	private List<ConcreteOrder> orders;

	public Order(String column, OrderType type) {
		this(new ConcreteOrder(column, type));
	}

	public Order(ConcreteOrder[] orders) {
		this.orders = new ArrayList<>(Arrays.asList(orders));
	}

	public Order(ConcreteOrder order) {
		this(new ConcreteOrder[]{order});
	}

	public Order() {
		this.orders = new ArrayList<>();
	}

	public void addOrder(ConcreteOrder order) {
		orders.add(order);
	}

	public ConcreteOrder[] getOrders() {
		return orders.toArray(new ConcreteOrder[0]);
	}

	public boolean hasOrder() {
		return !orders.isEmpty();
	}
}
