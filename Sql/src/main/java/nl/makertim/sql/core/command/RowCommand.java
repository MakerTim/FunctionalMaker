package nl.makertim.sql.core.command;

import java.util.Map;

public interface RowCommand<S extends Command<S>> extends ParameterCommand<S> {

	void addValue(String column, Object value);

	Map<String, Object> getValues();

}
