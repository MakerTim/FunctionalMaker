package nl.makertim.sql.core.types;

public class Int extends AbstractSimpleColumn {

	public Int(String columnName, String defaultValue) {
		super(columnName, 10, Type.INTEGER, defaultValue);
	}

	public Int(String columnName, int size, String defaultValue) {
		super(columnName, size, Type.INTEGER, defaultValue);
	}
}
