package nl.makertim.sql.core.types;

public class ColumnFactory {

	public static Column createFrom(String name, Type type, String size, String defaultValues) {
		String empty = "0";
		switch (type) {
		case INTEGER:
			if (size.equals(empty))
				return new Int(name, defaultValues);
			else
				return new Int(name, (int)java.lang.Double.parseDouble(size), defaultValues);
		case DECIMAL:
			if (size.equals(empty))
				return new Double(name, defaultValues);
			else
				return new Double(name, size, defaultValues);
		case CHAR:
			if (size.equals(empty))
				return new Char(name, defaultValues);
			else
				return new Char(name, (int)java.lang.Double.parseDouble(size), defaultValues);
		case VARCHAR:
			if (size.equals(empty))
				return new VarChar(name, defaultValues);
			else
				return new VarChar(name, (int)java.lang.Double.parseDouble(size), defaultValues);
		case TEXT:
			return new Text(name, defaultValues);
		case BLOB:
			if (size.equals(empty))
				return new Blob(name, defaultValues);
			else
				return new LongBlob(name, defaultValues);
		case TIMESTAMP:
			return new TimeStamp(name, defaultValues);
		case DATETIME:
			return new DateTime(name, defaultValues);
		case DATE:
			return new Date(name, defaultValues);
		case TIME:
			return new Time(name, defaultValues);
		case YEAR:
			return new Year(name, defaultValues);
		case CUSTOM:
		default:
			return new CustomColumn(name, "UNKNOWN", size, defaultValues);
		}
	}

}
