package nl.makertim.sql.core.where;

public class WhereNotBetween extends WhereBetween {

	public WhereNotBetween(String column, Object lowValue, Object highValue) {
		super(column, lowValue, highValue);
		compare = ConcreteWhereType.NOT_BETWEEN;
	}
}
