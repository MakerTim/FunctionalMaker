package nl.makertim.sql.core.command;

import nl.makertim.sql.core.order.Order;

public interface OrderedCommand<S extends Command<S>> {

	Order getOrder();

}
