package nl.makertim.sql.core.translator;


import nl.makertim.sql.core.command.ParameterCommand;
import nl.makertim.sql.core.where.*;

public interface WhereTranslator {

	default String parseWhere(ParameterCommand<?> cmd, Where where) {
		String sql = toSql(cmd, where);
		if (sql == null || sql.isEmpty()) {
			return "";
		}
		return "WHERE " + sql;
	}


	default String toSql(ParameterCommand<?> cmd, Where where) {
		if (where instanceof ConcreteWhere) {
			return concreteSql(cmd, (ConcreteWhere) where);
		}
		if (where instanceof CombineWhere) {
			return combineWhere(cmd, (CombineWhere) where);
		}
		return "";
	}

	default String concreteSql(ParameterCommand<?> cmd, ConcreteWhere concreteWhere) {
		switch (concreteWhere.getCompare()) {
		case LIKE:
			return toLikeSql(cmd, (WhereLike) concreteWhere);
		case BIGGER:
			return toBiggerSql(cmd, (WhereBigger) concreteWhere);
		case BIGGER_EQUALS:
			return toBiggerEqualsSql(cmd, (WhereBiggerEquals) concreteWhere);
		case SMALLER:
			return toSmallerSql(cmd, (WhereSmaller) concreteWhere);
		case SMALLER_EQUALS:
			return toSmallerEqualsSql(cmd, (WhereSmallerEquals) concreteWhere);
		case BETWEEN:
			return toBetweenSql(cmd, (WhereBetween) concreteWhere);
		case NOT_BETWEEN:
			return toNotBetweenSql(cmd, (WhereNotBetween) concreteWhere);
		case NOT:
			return toNotSql(cmd, (WhereNot) concreteWhere);
		case IN:
			return toInSql(cmd, (WhereIn) concreteWhere);
		case EQUALS:
		default:
			return toEqualsSql(cmd, (WhereEquals) concreteWhere);
		}
	}

	default String combineWhere(ParameterCommand<?> cmd, CombineWhere combineWhere) {
		if (combineWhere.getLeft() == null || combineWhere.getLeft() == Where.NONE) {
			if (combineWhere.getRight() == null || combineWhere.getRight() == Where.NONE) {
				return "";
			} else {
				return toSql(cmd, combineWhere.getRight());
			}
		} else if (combineWhere.getRight() == null || combineWhere.getRight() == Where.NONE) {
			return toSql(cmd, combineWhere.getLeft());
		}

		switch (combineWhere.getType()) {
		case OR:
			return combineOR(cmd, (CombineWhereOR) combineWhere);
		case XOR:
			return combineXOR(cmd, (CombineWhereXOR) combineWhere);
		case NOT:
			return combineNot(cmd, (CombineWhereNot) combineWhere);
		case AND:
		default:
			return combineAND(cmd, (CombineWhereAND) combineWhere);
		}
	}

	String combineAND(ParameterCommand<?> cmd, CombineWhereAND combineWhere);

	String combineOR(ParameterCommand<?> cmd, CombineWhereOR combineWhere);

	String combineXOR(ParameterCommand<?> cmd, CombineWhereXOR combineWhere);

	String combineNot(ParameterCommand<?> cmd, CombineWhereNot combineWhere);

	String toSmallerSql(ParameterCommand<?> cmd, WhereSmaller where);

	String toSmallerEqualsSql(ParameterCommand<?> cmd, WhereSmallerEquals where);

	String toNotBetweenSql(ParameterCommand<?> cmd, WhereNotBetween where);

	String toLikeSql(ParameterCommand<?> cmd, WhereLike where);

	String toInSql(ParameterCommand<?> cmd, WhereIn where);

	String toBetweenSql(ParameterCommand<?> cmd, WhereBetween where);

	String toNotSql(ParameterCommand<?> cmd, WhereNot where);

	String toEqualsSql(ParameterCommand<?> cmd, WhereEquals where);

	String toBiggerSql(ParameterCommand<?> cmd, WhereBigger where);

	String toBiggerEqualsSql(ParameterCommand<?> cmd, WhereBiggerEquals where);

}
