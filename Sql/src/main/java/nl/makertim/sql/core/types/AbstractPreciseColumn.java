package nl.makertim.sql.core.types;

public abstract class AbstractPreciseColumn implements Column {

	protected String columnName;
	protected String length;
	protected Type type;
	protected String defaultValue;

	public AbstractPreciseColumn(String columnName, String length, Type type, String defaultValue) {
		this.columnName = columnName;
		this.length = length;
		this.type = type;
		this.defaultValue = defaultValue;
	}

	@Override
	public String columnName() {
		return columnName;
	}

	@Override
	public Type type() {
		return type;
	}

	@Override
	public String getLength() {
		return length;
	}

	@Override
	public String defaultValue() {
		return defaultValue;
	}

	@Override
	public String toString() {
		return str();
	}
}
