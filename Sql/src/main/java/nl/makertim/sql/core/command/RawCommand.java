package nl.makertim.sql.core.command;

@Deprecated
@SuppressWarnings("DeprecatedIsStillUsed")
public class RawCommand implements Command<RawCommand> {

	protected String raw;

	public RawCommand(String rawSQL) {
		this.raw = rawSQL;
	}

	public String getRawSQL() {
		return raw;
	}

	@Override
	public String getTable() {
		return null;
	}

	@Override
	public CommandType<RawCommand> getType() {
		return CommandType.UNKNOWN;
	}
}




