package nl.makertim.sql.core.types;

public class Char extends AbstractSimpleColumn {

	public Char(String columnName, String defaultValue) {
		super(columnName, 36, Type.CHAR, defaultValue);
	}

	public Char(String columnName, int length, String defaultValue) {
		super(columnName, length, Type.CHAR, defaultValue);
	}

}
