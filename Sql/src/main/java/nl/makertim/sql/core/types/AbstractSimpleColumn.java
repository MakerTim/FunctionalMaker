package nl.makertim.sql.core.types;

public abstract class AbstractSimpleColumn implements Column {

	protected String columnName;
	protected int length;
	protected Type type;
	protected String defaultValue;

	public AbstractSimpleColumn(String columnName, int length, Type type, String defaultValue) {
		this.columnName = columnName;
		this.length = length;
		this.type = type;
		this.defaultValue = defaultValue;
	}

	@Override
	public String columnName() {
		return columnName;
	}

	@Override
	public Type type() {
		return type;
	}

	@Override
	public String getLength() {
		return String.valueOf(length);
	}

	@Override
	public String defaultValue() {
		return defaultValue;
	}

	@Override
	public String toString() {
		return str();
	}
}
