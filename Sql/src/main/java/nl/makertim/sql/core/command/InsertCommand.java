package nl.makertim.sql.core.command;

import java.util.*;

public class InsertCommand extends AbstractCommand<InsertCommand>
		implements RowCommand<InsertCommand>, ParameterCommand<InsertCommand>, ColumnsSelectableCommand<InsertCommand> {

	private Map<String, Object> values = new LinkedHashMap<>();
	private List<Object> variables = new ArrayList<>();

	public InsertCommand(String table, ColumnValue... cValues) {
		super(table);
		for (ColumnValue cValue : cValues) {
			this.values.put(cValue.column, cValue.value);
		}
	}

	@Override
	public CommandType<InsertCommand> getType() {
		return CommandType.INSERT;
	}

	public void addValue(InsertCommand.ColumnValue cValue) {
		this.addValue(cValue.column, cValue.value);
	}

	@Override
	public void addValue(String column, Object value) {
		values.put(column, value);
	}

	@Override
	public Map<String, Object> getValues() {
		return Collections.unmodifiableMap(values);
	}

	@Override
	public String[] getColumns() {
		return values.keySet().toArray(new String[0]);
	}

	@Override
	public void addParameter(Object value) {
		variables.add(value);
	}

	@Override
	public Object[] getParameters() {
		return variables.toArray(new Object[0]);
	}

	public static class ColumnValue {
		private String column;
		private Object value;

		public ColumnValue(String column, Object value) {
			this.column = column;
			this.value = value;
		}
	}

}
