package nl.makertim.sql.core.types;

public interface Column {

	String columnName();

	Type type();

	String getLength();

	String defaultValue();

	default String str() {
		return columnName() + " " + type() + "(" + getLength() + ") " + defaultValue();
	}
}
