package nl.makertim.sql.core.where;

public class WhereBetween extends ConcreteWhere {

	private Object small;
	private Object big;

	public WhereBetween(String column, Object valueSmall, Object valueBig) {
		super(column, ConcreteWhereType.BETWEEN, null);
		this.small = valueSmall;
		this.big = valueBig;
	}

	@Override
	public Object getValue() {
		throw new IllegalArgumentException();
	}

	public Object getSmall() {
		return small;
	}

	public Object getBig() {
		return big;
	}
}
