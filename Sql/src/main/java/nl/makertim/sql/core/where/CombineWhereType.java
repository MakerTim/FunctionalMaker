package nl.makertim.sql.core.where;

public enum CombineWhereType {
	AND, OR, XOR, NOT
}
