package nl.makertim.sql.core.where;

public class WhereBigger extends ConcreteWhere {

	public WhereBigger(String column, Object value) {
		super(column, ConcreteWhereType.BIGGER, value);
	}
}
