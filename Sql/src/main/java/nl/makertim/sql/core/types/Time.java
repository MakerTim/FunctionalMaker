package nl.makertim.sql.core.types;

public class Time extends AbstractSimpleColumn {

	public Time(String columnName, String defaultValue) {
		super(columnName, 0, Type.TIME, defaultValue);
	}

}
