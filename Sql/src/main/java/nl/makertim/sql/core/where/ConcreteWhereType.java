package nl.makertim.sql.core.where;

public enum ConcreteWhereType {
	EQUALS,
	LIKE,
	BIGGER,
	BIGGER_EQUALS,
	SMALLER,
	SMALLER_EQUALS,
	BETWEEN,
	NOT_BETWEEN,
	NOT,
	IN,
}
