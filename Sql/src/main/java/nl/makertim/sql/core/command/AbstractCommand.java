package nl.makertim.sql.core.command;

public abstract class AbstractCommand<C extends AbstractCommand<C>> implements Command<C> {

	private String table;

	public AbstractCommand(String table) {
		this.table = table;
	}

	@Override
	public String getTable() {
		return table;
	}
}
