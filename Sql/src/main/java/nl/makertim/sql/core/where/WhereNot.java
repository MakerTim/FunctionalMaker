package nl.makertim.sql.core.where;

public class WhereNot extends ConcreteWhere {

	public WhereNot(String column, Object value) {
		super(column, ConcreteWhereType.NOT, value);
	}
}
