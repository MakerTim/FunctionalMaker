package nl.makertim.sql.core.types;

public class DateTime extends AbstractSimpleColumn {

	public DateTime(String columnName, String defaultValue) {
		super(columnName, 0, Type.DATETIME, defaultValue);
	}

}
