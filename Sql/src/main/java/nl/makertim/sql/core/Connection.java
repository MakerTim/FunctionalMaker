package nl.makertim.sql.core;

import nl.makertim.functionalmaker.Try;
import nl.makertim.functionalmaker.functionalinterface.CatchConsumer;
import nl.makertim.sql.core.command.Command;
import nl.makertim.sql.core.command.ParameterCommand;
import nl.makertim.sql.core.command.StructureCommand;
import nl.makertim.sql.core.translator.CommandTranslator;
import nl.makertim.sql.core.types.Column;

import java.sql.*;
import java.util.Date;
import java.util.function.Consumer;

public abstract class Connection implements Runnable {

	protected java.sql.Connection connection;
	private CommandTranslator translator;
	private int delay;
	private final int port;
	private final String type;
	private final String username;
	private final String password;
	private final String database;
	private final String server;
	private final String fullUrl;

	protected int defaultTimer = 100;
	protected Consumer<Throwable> onError = Throwable::printStackTrace;

	protected Connection(String type, String server, int port, String username, String password, String database) {
		Try.execute(() -> Class.forName(getDriverClass())).orThrow(RuntimeException::new);
		this.port = port;
		this.type = type;
		this.database = database.indexOf('?') > -1 ? database.substring(0, database.indexOf('?')) : database;
		this.username = username;
		this.password = password;
		this.server = server;
		if (port == -1) {
			this.fullUrl = String.format("jdbc:%s://%s/%s", type, server, database);
		} else {
			this.fullUrl = String.format("jdbc:%s://%s:%d/%s", type, server, port, database);
		}
		this.translator = getNewTranslator();
		new Thread(this).start();
	}

	protected abstract String getDriverClass();

	protected abstract CommandTranslator getNewTranslator();

	public void setOnError(Consumer<Throwable> onError) {
		this.onError = onError;
	}

	public Consumer<Throwable> getOnError() {
		return onError;
	}

	public void neverDisconnect() {
		delay = -1;
		defaultTimer = -1;
	}

	public String getDriverVersion() {
		return Try //
				.execute(() -> connection.getMetaData().getDriverVersion()) //
				.onFail(onError) //
				.orElse(() -> "-1") //
				.getData();
	}

	public abstract String getServerVersion();

	public boolean openConnection() {
		return Try //
				.execute(() -> {
					connection = DriverManager.getConnection(fullUrl, username, password);
					return true;
				}) //
				.onFail(onError) //
				.orElse(() -> false).getData();
	}

	public boolean closeConnection() {
		return Try
				.execute(() -> {
					if (connection != null) {
						connection.close();
						return true;
					}
					return false;
				})//
				.always(() -> connection = null) //
				.onFail(onError) //
				.orElse(() -> false).getData();
	}

	public String toSql(Command command) {
		return translator.toSql(command);
	}

	protected void fillInParameters(Command cmd, PreparedStatement statement) {
		if (!(cmd instanceof ParameterCommand)) {
			return;
		}
		ParameterCommand command = (ParameterCommand) cmd;

		Object[] parameters = command.getParameters();
		for (int i = 0; i < parameters.length; i++) {
			final int I = i;
			Try.execute(() -> {
				Object obj = parameters[I];
				if (obj instanceof Date) {
					obj = new java.sql.Date(((Date) obj).getTime());
				}
				statement.setObject(I + 1, obj);
			});
		}
	}

	public Column[] reverse(StructureCommand command) {
		return translator.reverse(execute(command));
	}

	public ResultSet execute(Command command) {
		switch (command.getType().getInnerType()) {
		case SELECT:
			return selectQuery(command);
		case UPDATE:
			return updateQuery(command);
		case DELETE:
			return deleteQuery(command);
		case INSERT:
			return insertQuery(command);
		case CREATE:
			return insertQuery(command);
		case ALTER:
			return insertQuery(command);
		case DROP:
			return insertQuery(command);
		case TRUNCATE:
			return insertQuery(command);
		case GET_STRUCTURE:
			return selectQuery(command);
		case UNKNOWN:
		default:
			return selectQuery(command);
		}
	}

	protected ResultSet executePrepared(Command command, CatchConsumer<PreparedStatement, SQLException> prepare) {
		String sql = toSql(command);
		return Try
				.execute(() -> {
					openIfNotClosed();
					PreparedStatement statement = connection.prepareStatement(sql);
					fillInParameters(command, statement);
					prepare.accept(statement);
					return statement.executeQuery();
				}) //
				.onFail(err -> System.err.println(sql)) //
				.onFail(onError) //
				.getData();
	}

	protected ResultSet updatePrepared(Command command, CatchConsumer<PreparedStatement, SQLException> prepare) {
		String sql = toSql(command);
		return Try
				.execute(() -> {
					openIfNotClosed();
					PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					fillInParameters(command, statement);
					prepare.accept(statement);
					statement.executeUpdate();
					return statement.getGeneratedKeys();
				}) //
				.onFail(err -> System.err.println(sql)) //
				.onFail(onError) //
				.getData();
	}

	protected ResultSet selectQuery(Command command) {
		return executePrepared(command, preparedStatement -> {
		});
	}

	protected ResultSet insertQuery(Command command) {
		return updatePrepared(command, preparedStatement -> {
		});
	}

	protected ResultSet updateQuery(Command command) {
		return insertQuery(command);
	}

	protected ResultSet deleteQuery(Command command) {
		return insertQuery(command);
	}

	private void openIfNotClosed() {
		Try.execute(() -> {
			renewTimer();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT 1;");
			preparedStatement.executeQuery();
		}).orElse(this::openConnection);
	}

	private void renewTimer() {
		delay = defaultTimer;
	}

	@Override
	public void run() {
		if (delay-- == 0) {
			closeConnection();
		}
		Try.execute(() -> Thread.sleep(1000L));
	}
}
