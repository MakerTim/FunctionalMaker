package nl.makertim.sql.core.types;

public class Text extends AbstractSimpleColumn {

	public Text(String columnName, String defaultValue) {
		super(columnName, 0, Type.TEXT, defaultValue);
	}

}
