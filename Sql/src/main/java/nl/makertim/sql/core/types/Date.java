package nl.makertim.sql.core.types;

public class Date extends AbstractSimpleColumn {

	public Date(String columnName, String defaultValue) {
		super(columnName, 0, Type.DATE, defaultValue);
	}

}
