package nl.makertim.sql.core.command;

public interface OffsetCommand<S extends Command<S>> extends LimitedCommand<S> {

	// -1 = nostart
	int getStart();

	void setStart(int start);

	void setStartAndRows(int start, int rows);
}
