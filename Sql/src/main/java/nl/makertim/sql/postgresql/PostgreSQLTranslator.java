package nl.makertim.sql.postgresql;

import nl.makertim.sql.core.command.*;
import nl.makertim.sql.core.translator.*;
import nl.makertim.sql.core.types.Column;
import nl.makertim.sql.core.types.Type;
import nl.makertim.sql.core.where.CombineWhereXOR;

public class PostgreSQLTranslator implements CommandTranslator, SQLWhereTranslator, SQLTypeTranslator {

	@Override
	public char getQuote() {
		return '"';
	}

	@Override
	public String escapeQuote() {
		return "\"\"";
	}

	@Override
	public char stringQuote() {
		return '\'';
	}

	@Override
	public String escapeStringQuote() {
		return "''";
	}

	@Override
	public char separator() {
		return ',';
	}

	@Override
	public WhereTranslator getWhereTranslator() {
		return this;
	}

	@Override
	public TypeTranslator getTypeTranslator() {
		return this;
	}

	@Override
	public CommandTranslator getTranslator() {
		return this;
	}

	@Override
	public String getSelectFormat() {
		return "SELECT {{distinct}} {{columns}} FROM {{table}} {{where}} {{order}} {{limit}}";
	}

	@Override
	public String getUpdateFormat() {
		return "UPDATE {{table}} SET {{column-values}} {{where}}";
	}

	@Override
	public String getDeleteFormat() {
		return "DELETE FROM {{table}} {{where}}";
	}

	@Override
	public String getInsertFormat() {
		return "INSERT INTO {{table}} {{column}} VALUES {{values}}";
	}

	@Override
	public String getCreateFormat() {
		return "CREATE TABLE {{exists}} {{table}} ({{columns-typed}})";
	}

	@Override
	public String getDropFormat() {
		return "DROP TABLE {{table}}";
	}

	@Override
	public String getTruncateFormat() {
		return "DELETE FROM {{table}}";
	}

	@Override
	public String getAlterFormat() {
		return "ALTER TABLE {{table}} ALTER COLUMN {{column}} TYPE {{type}};";
	}

	@Override
	public String parseLimit(LimitedCommand<?> command) {
		if (command.getRows() > 0) {
			String ret = " LIMIT " + command.getRows();

			if (command instanceof OffsetCommand && ((OffsetCommand) command).getStart() > 0) {
				ret += "OFFSET " + ((OffsetCommand) command).getStart();
			}
			return ret;
		}
		return "";
	}

	@Override
	public String toAlterSql(AlterCommand command) {
		String alterFormat = getAlterFormat();
		if (!command.getOldColumnName().equals(command.getNewColumn().columnName())) {
			alterFormat += "ALTER TABLE {{table}} RENAME {{column}} TO {{new-column}}";
		}

		String[] nameTypeDefault = getTypeTranslator().toSql(command.getNewColumn()).split(" ");
		return alterFormat
				.replace("{{table}}", safeName(command.getTable()))
				.replace("{{column}}", safeName(command.getOldColumnName()))
				.replace("{{type}}", nameTypeDefault[1])

				.replace("{{table}}", safeName(command.getTable()))
				.replace("{{column}}", safeName(command.getOldColumnName()))
				.replace("{{new-column}}", nameTypeDefault[0])
				;
	}

	@Override
	public String toUpdateSql(UpdateCommand command) {
		if (command.getRows() > 0) {
			System.err.println("There is no LIMIT in PostgreSQL UPDATE command, ignoring");
			// TODO: make a workaround for this... somehow
			command.setRows(0);
		}
		return CommandTranslator.super.toUpdateSql(command);
	}

	@Override
	public String toDeleteSql(DeleteCommand command) {
		if (command.getRows() > 0) {
			System.err.println("There is no LIMIT in PostgreSQL DELETE command, ignoring");
			// TODO: make a workaround for this... somehow
			command.setRows(0);
		}
		return CommandTranslator.super.toDeleteSql(command);
	}

	@Override
	public String combineXOR(ParameterCommand<?> cmd, CombineWhereXOR combineWhere) {
		// (NOT left AND right) OR (NOT right AND left)
		return "(NOT " + toSql(cmd, combineWhere.getLeft()) + " AND " + toSql(cmd, combineWhere.getRight()) + ")" +
				"OR" +
				"(NOT " + toSql(cmd, combineWhere.getRight()) + " AND " + toSql(cmd, combineWhere.getLeft()) + ")"
				;
	}

	@Override
	public String integerType(Column column) {
		return getTranslator().safeName(column.columnName()) + " INTEGER" + " " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	public String datetimeType(Column column) {
		return getTranslator().safeName(column.columnName()) + " TIMESTAMP " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	public String yearType(Column column) {
		return getTranslator().safeName(column.columnName()) + " SMALLINT " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	public String blobType(Column column) {
		return getTranslator().safeName(column.columnName()) + " BYTEA " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	public String bigblobType(Column column) {
		return blobType(column);
	}

	@Override
	public Type reverse(String name) {
		if (name.equals("character varying")) {
			name = "varchar";
		} else if (name.equals("character")) {
			name = "char";
		}
		name = name.split(" ")[0];
		return SQLTypeTranslator.super.reverse(name);
	}
}
