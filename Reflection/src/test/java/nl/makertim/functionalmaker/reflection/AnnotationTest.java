package nl.makertim.functionalmaker.reflection;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AnnotationTest {

	@Test
	public void findTestAnnotationClassesAll() {
		List<Class> classList = new ArrayList<>(ClassFinder.getClassesInWithAnnotation(TestAnnotation.class));

		Assert.assertSame(1, classList.size());
	}

	@Test
	public void findTestAnnotationClasses() {
		String packageName = getClass().getName().replace("." + getClass().getSimpleName(), "");
		List<Class> classList = new ArrayList<>(
				ClassFinder.getClassesInWithAnnotation(packageName, TestAnnotation.class));
		Assert.assertSame(1, classList.size());
	}

	@Test
	public void findTestAnnotationFieldsAll() {
		Collection<Field> fields = FieldFinder.getFieldsOfClass(TestClass.class);
		Assert.assertSame(3, fields.size());

		fields = FieldFinder.getFieldsOfClass(TestExtendsClass.class);
		Assert.assertSame(6, fields.size());

		fields = FieldFinder.getFieldsOfClass(TestExtendsClass.class, false);
		Assert.assertSame(3, fields.size());
	}

	@Test
	public void findTestAnnotationFieldsFiltered() {
		Collection<Field> fields = FieldFinder.getFieldsWithAnnotationOfClass(TestClass.class, TestAnnotation.class);
		Assert.assertSame(3, fields.size());

		fields = FieldFinder.getFieldsWithAnnotationOfClass(TestExtendsClass.class, TestAnnotation.class);
		Assert.assertSame(6, fields.size());

		fields = FieldFinder.getFieldsWithAnnotationOfClass(TestExtendsClass.class, TestAnnotation.class, false);
		Assert.assertSame(3, fields.size());
	}

	@Test
	public void findTestAnnotationMethodsAll() {
		Collection<Method> methods = MethodFinder.getMethodsOfClass(TestClass.class);
		Assert.assertTrue(methods.size() > 3);

		methods = MethodFinder.getMethodsOfClass(TestExtendsClass.class);
		Assert.assertTrue(methods.size() > 6);

		methods = MethodFinder.getMethodsOfClass(TestExtendsClass.class, false);
		Assert.assertEquals(3, methods.size());
	}

	@Test
	public void findTestAnnotationMethodsFiltered() {
		Collection<Method> methods = MethodFinder.getMethodsWithAnnotationOfClass(TestClass.class, TestAnnotation.class);
		Assert.assertSame(3, methods.size());

		methods = MethodFinder.getMethodsWithAnnotationOfClass(TestExtendsClass.class, TestAnnotation.class);
		Assert.assertSame(6, methods.size());

		methods = MethodFinder.getMethodsWithAnnotationOfClass(TestExtendsClass.class, TestAnnotation.class, false);
		Assert.assertSame(3, methods.size());
	}

	@Test
	public void testAllTogether() {
		Collection<Class> classes = ClassFinder.getClassesIn("nl.makertim.functionalmaker.reflection");
		Collection<Field> allFields = new ArrayList<>();
		Collection<Field> allFieldsSuper = new ArrayList<>();
		classes.forEach(cls ->{
			allFields.addAll(FieldFinder.getFieldsWithAnnotationOfClass(cls, TestAnnotation.class, false));
			allFieldsSuper.addAll(FieldFinder.getFieldsWithAnnotationOfClass(cls, TestAnnotation.class, true));
		});

		Assert.assertEquals(6, allFields.size());
		Assert.assertEquals(9, allFieldsSuper.size());
	}
}
