package nl.makertim.functionalmaker.reflection;

@TestAnnotation
public class TestClass {

	@TestAnnotation
	private String x;
	@TestAnnotation
	private String y;
	@TestAnnotation
	private String z;

	@TestAnnotation
	private void k() {
	}

	@TestAnnotation
	private void l() {
	}

	@TestAnnotation
	private void m() {
	}

}
