package nl.makertim.functionalmaker.reflection;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Objects;

public class AnnotationFinder {

	private AnnotationFinder() {
	}

	/**
	 * Don't forget, only annotations with {@link java.lang.annotation.RetentionPolicy}.RUNTIME work!!
	 */
	public static boolean hasAnnotation(Annotation[] hasAnnotations, Class<? extends Annotation> annotationToFind) {
		return Arrays.stream(hasAnnotations)
				.anyMatch(annotation -> Objects.equals(annotation.annotationType(), annotationToFind));
	}

}
