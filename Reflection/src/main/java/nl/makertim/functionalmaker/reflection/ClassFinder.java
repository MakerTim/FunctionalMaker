package nl.makertim.functionalmaker.reflection;

import nl.makertim.functionalmaker.Try;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ClassFinder {

	private ClassFinder() {
	}

	/**
	 * Don't forget, only annotations with {@link java.lang.annotation.RetentionPolicy}.RUNTIME work!!
	 */
	public static Collection<Class> getClassesInWithAnnotation(Class<? extends Annotation> annotation) {
		return getClassesInWithAnnotation("", annotation);
	}

	/**
	 * Don't forget, only annotations with {@link java.lang.annotation.RetentionPolicy}.RUNTIME work!!
	 */
	public static Collection<Class> getClassesInWithAnnotation(String packageName, Class<? extends Annotation> annotation) {
		return Try
				.execute(() -> getClassesInWithAnnotationRaw(packageName, annotation))
				.orElse((Supplier<Collection<Class>>) ArrayList::new)
				.getData();
	}

	public static Collection<Class> getClassesIn() {
		return getClassesIn("");
	}

	public static Collection<Class> getClassesIn(String packageName) {
		return Try
				.execute(() -> getClassesInRaw(packageName))
				.orElse((Supplier<Collection<Class>>) ArrayList::new)
				.getData();
	}

	/**
	 * Don't forget, only annotations with {@link java.lang.annotation.RetentionPolicy}.RUNTIME work!!
	 */
	public static Collection<Class> getClassesInWithAnnotationRaw(String packageName, Class<? extends Annotation> annotation) throws IOException, URISyntaxException {
		return getClassesInRaw(packageName).stream()
				.filter(cls ->
						AnnotationFinder.hasAnnotation(cls.getAnnotations(), annotation)
				)
				.collect(Collectors.toSet());
	}

	public static Collection<Class> getClassesInRaw() throws IOException, URISyntaxException {
		return getClassesInRaw("");
	}

	public static Collection<Class> getClassesInRaw(String packageName) throws IOException, URISyntaxException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		Set<File> dirs = new LinkedHashSet<>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			URI uri = new URI(resource.toString());
			String uriPath = uri.getPath();
			if (uriPath != null) {
				dirs.add(new File(uriPath));
			}
		}
		Set<Class> classes = new HashSet<>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return classes;
	}

	public static Collection<Class> findClasses(File directory, String packageName) {
		String classExtension = ".class";

		Set<Class> classes = new HashSet<>();
		File[] files;
		if (!directory.exists() || (files = directory.listFiles()) == null) {
			return classes;
		}
		for (File file : files) {
			if (file.isDirectory()) {
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(classExtension)) {
				Class cls = findOrLoad((packageName + '.' + file.getName().replaceAll(classExtension + "$", "")).replaceAll("^\\.", ""));
				if (cls != null) {
					classes.add(cls);
				}
			}
		}
		return classes;
	}

	public static Class findOrLoad(String className) {
		Class cls;
		try {
			cls = Class.forName(className);
		} catch (Exception ex) {
			cls = null;
		}
		return cls;
	}

	@SuppressWarnings("unchecked")
	public static <T> Class<? extends T> findOrLoadTyped(String className) {
		Class<? extends T> cls;
		try {
			cls = (Class<? extends T>) Class.forName(className);
		} catch (Exception ex) {
			cls = null;
		}
		return cls;
	}
}
