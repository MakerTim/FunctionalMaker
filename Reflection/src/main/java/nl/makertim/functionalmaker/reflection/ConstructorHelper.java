package nl.makertim.functionalmaker.reflection;

import nl.makertim.functionalmaker.Try;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Consumer;

public class ConstructorHelper {

	private ConstructorHelper() {
	}

	@SuppressWarnings("unchecked")
	public static <T> Optional<T> createInstance(Class<T> tClass, Consumer<Throwable> onError, Object... params) {
		return Optional.ofNullable(
				Try.execute(() -> {
					Class<?>[] paramTypes = Arrays.stream(params).map(Object::getClass).toArray(Class[]::new);

					Constructor<?>[] constructors = tClass.getDeclaredConstructors();
					Constructor<T> theConstructor = null;
					for (Constructor<?> constructor : constructors) {
						Class<?>[] paramTypesNeeded = constructor.getParameterTypes();
						if (paramTypes.length != paramTypesNeeded.length) {
							continue;
						}
						boolean found = true;
						for (int i = 0; i < paramTypes.length; i++) {
							if (!paramTypesNeeded[i].isAssignableFrom(paramTypes[i])) {
								found = false;
								break;
							}
						}
						if (found) {
							theConstructor = (Constructor<T>) constructor;
						}
					}
					if (theConstructor == null) {
						throw new NoSuchMethodException("No matching constructor found!");
					}
					return theConstructor.newInstance(params);
				}).onFail(onError)
						.getData());
	}

}
