package nl.makertim.functionalmaker.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class FieldFinder {

	private FieldFinder() {
	}

	public static Collection<Field> getFieldsOfClass(Class cls) {
		return getFieldsOfClass(cls, true);
	}

	public static Collection<Field> getFieldsOfClass(Class cls, boolean includeSuperclassFields) {
		Collection<Field> fields = new LinkedHashSet<>(Arrays.asList(cls.getDeclaredFields()));

		if (cls.getSuperclass() != null && includeSuperclassFields) {
			fields.addAll(getFieldsOfClass(cls.getSuperclass(), true));
		}

		return fields;
	}

	public static Collection<Field> getFieldsWithAnnotationOfClass(Class cls, Class<? extends Annotation> annotation) {
		return getFieldsWithAnnotationOfClass(cls, annotation, true);
	}

	public static Collection<Field> getFieldsWithAnnotationOfClass(Class cls, Class<? extends Annotation> annotation, boolean includeSuperclassFields) {
		Collection<Field> fields = getFieldsOfClass(cls, includeSuperclassFields);

		return fields.stream() //
				.filter(field -> AnnotationFinder.hasAnnotation(field.getAnnotations(), annotation))
				.collect(Collectors.toList());
	}
}
