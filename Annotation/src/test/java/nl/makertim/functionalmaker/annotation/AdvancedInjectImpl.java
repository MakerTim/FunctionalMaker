package nl.makertim.functionalmaker.annotation;

public class AdvancedInjectImpl {

	@Inject
	private static AdvancedSingletonImpl singleton;

	static {
		Processor.instance.injectStatic(AdvancedInjectImpl.class);
	}

	public AdvancedSingletonImpl getSingleton() {
		return singleton;
	}
}
