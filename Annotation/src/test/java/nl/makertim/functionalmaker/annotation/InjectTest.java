package nl.makertim.functionalmaker.annotation;

import org.junit.Assert;
import org.junit.Test;

public class InjectTest {

	@Test
	public void testInjection() {
		BasicInjectImpl injectCls = new BasicInjectImpl();

		Assert.assertNotNull(injectCls.getSingleton());
		Assert.assertNotNull(injectCls.getSingletonInstance());
	}

	@Test
	public void testNestedInjection() {
		BasicInjectNestedImpl injectCls = new BasicInjectNestedImpl();

		Assert.assertNotNull(injectCls.getSingleton());
		Assert.assertNotNull(injectCls.getSingleton2());
	}

	@Test
	public void testUserInjection() {
		UserInjectImpl injectCls = new UserInjectImpl();

		Assert.assertNotNull(injectCls.getSingleton());
		Assert.assertNotNull(injectCls.getSingletonInstance());
	}

	@Test
	public void testUserNestedInjection() {
		UserNestedInjectImpl injectCls = new UserNestedInjectImpl();

		Assert.assertNotNull(injectCls.getSingleton());
		Assert.assertNotNull(injectCls.getSingletonInstance());
	}

	@Test
	public void testInjectionAdvancedParameter() {
		AdvancedInjectImpl injectCls = new AdvancedInjectImpl();
		AdvancedSingletonImpl advancedSingleton = injectCls.getSingleton();

		Assert.assertNotNull(advancedSingleton);
		Assert.assertNotNull(advancedSingleton.getSingleton());
	}

	@Test
	public void testExample() {
		new SingletonUserExample();
	}
}
