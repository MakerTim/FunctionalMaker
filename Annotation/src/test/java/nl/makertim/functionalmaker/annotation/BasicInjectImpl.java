package nl.makertim.functionalmaker.annotation;

public class BasicInjectImpl {

	@Inject
	private static BasicSingletonImpl singleton;

	@Inject
	private BasicSingletonImpl singletonInstance;

	static {
		Processor.instance.injectStatic(BasicInjectImpl.class);
	}

	public BasicInjectImpl(){
		Processor.instance.injectInstance(this);
	}

	public BasicSingletonImpl getSingleton() {
		return singleton;
	}

	public BasicSingletonImpl getSingletonInstance() {
		return singletonInstance;
	}
}
