package nl.makertim.functionalmaker.annotation;

public class UserInjectImpl extends SingletonUser {

	@Inject
	private static BasicSingletonImpl singleton;

	@Inject
	private BasicSingletonImpl singletonInstance;

	public BasicSingletonImpl getSingleton() {
		return singleton;
	}

	public BasicSingletonImpl getSingletonInstance() {
		return singletonInstance;
	}
}
