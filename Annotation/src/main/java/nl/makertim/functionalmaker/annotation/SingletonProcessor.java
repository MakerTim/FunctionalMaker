package nl.makertim.functionalmaker.annotation;

import nl.makertim.functionalmaker.Try;
import nl.makertim.functionalmaker.functionalinterface.CatchSupplier;
import nl.makertim.functionalmaker.reflection.AnnotationFinder;
import nl.makertim.functionalmaker.reflection.FieldFinder;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.*;

public class SingletonProcessor implements Processor {

	protected Map<Class, Object> singletonCache = new LinkedHashMap<>();

	@SuppressWarnings("unchecked")
	public static <T> T getInstanceOf(Class<T> type) {
		return (T) instance.getSingleton(type);
	}

	public void injectStatic(Class<?> injectClass) {
		inject(injectClass, null);
	}

	public void injectInstance(Object instance) {
		inject(instance.getClass(), instance);
	}

	protected void inject(Class<?> injectClass, Object object) {
		Collection<Field> fields = FieldFinder.getFieldsOfClass(injectClass);
		fields.stream()
				.filter(field ->
						AnnotationFinder.hasAnnotation(field.getAnnotations(), Inject.class))
				.filter(field -> Modifier.isStatic(field.getModifiers()) == Objects.isNull(object))
				.forEach(field -> {
					boolean isAccessible = field.isAccessible()/* JAVA10: field.canAccess(object) */;
					if (!isAccessible) {
						field.setAccessible(true);
					}
					Try
							.execute(() -> field.set(object, instance.getSingleton(field.getType())))
							.orThrow(RuntimeException::new);
					if (!isAccessible) {
						field.setAccessible(false);
					}
				});
	}

	public Object getSingleton(Class<?> type) {
		Object ret = singletonCache.get(type);
		if (ret == null) {
			ret = newInstance(type);
			singletonCache.put(type, ret);
		}
		return ret;
	}

	protected Object newInstance(Class<?> type) {
		return Try
				.execute(() -> {
					Constructor<?> constructor = findConstructor(type);
					return newInstance(constructor);
				})
				.orThrow(RuntimeException::new)
				.getData();
	}

	protected Object newInstance(Constructor<?> constructor) {
		Parameter[] parameters = constructor.getParameters();
		boolean isAccessible = openConstructor(constructor);

		if (parameters.length == 0) {
			return Try
					.execute((CatchSupplier<?, Throwable>) constructor::newInstance)
					.always(() -> closeConstructor(constructor, isAccessible))
					.orThrow(RuntimeException::new)
					.getData();
		}

		Object[] parametersObjects = new Object[parameters.length];
		for (int i = 0; i < parameters.length; i++) {
			Parameter parameter = parameters[i];
			Class<?> type = parameter.getType();
			parametersObjects[i] = getSingleton(type); // StackOverflowException
		}
		return Try
				.execute(() -> constructor.newInstance(parametersObjects))
				.always(() -> closeConstructor(constructor, isAccessible))
				.orThrow(RuntimeException::new)
				.getData();
	}

	protected Constructor<?> findConstructor(Class<?> type) {
		int params = -1;
		Constructor<?> foundConstructor = null;
		Constructor<?>[] constructors = type.getDeclaredConstructors();
		for (Constructor<?> constructor : constructors) {
			Parameter[] parameters = constructor.getParameters();

			boolean isInvalidConstructor = Arrays.stream(parameters)
					.anyMatch(parameter ->
							!AnnotationFinder.hasAnnotation(parameter.getAnnotations(), Inject.class) &&
									isSingleton(parameter.getType())
					);
			if (isInvalidConstructor) {
				continue;
			}
			if (params < parameters.length) {
				foundConstructor = constructor;
				params = parameters.length;
			}
		}
		return foundConstructor;
	}

	protected boolean openConstructor(Constructor<?> constructor) {
		boolean isAccessible = constructor.isAccessible()/* JAVA10: constructor.canAccess(null) */;
		if (!isAccessible) {
			constructor.setAccessible(true);
		}
		return isAccessible;
	}

	protected void closeConstructor(Constructor<?> constructor, boolean isAccessible) {
		if (!isAccessible) {
			constructor.setAccessible(false);
		}
	}

	protected boolean isSingleton(Class<?> type) {
		if (!AnnotationFinder.hasAnnotation(type.getAnnotations(), Singleton.class)) {
			throw new TypeIsNotASingletonException(String.format("Type `%s` is no @Singleton", type));
		}
		return true;
	}

	protected SingletonProcessor() {
	}
}
