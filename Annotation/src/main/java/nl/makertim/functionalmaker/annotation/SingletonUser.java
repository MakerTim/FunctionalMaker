package nl.makertim.functionalmaker.annotation;

import java.util.HashSet;
import java.util.Set;

public abstract class SingletonUser implements SingletonUserInterface {

	private static Set<Class<? extends SingletonUser>> registered = new HashSet<>();

	public SingletonUser() {
		if (!registered.contains(getClass())) {
			registered.add(getClass());
			Processor.instance.injectStatic(getClass());
		}

		register();
	}
}
