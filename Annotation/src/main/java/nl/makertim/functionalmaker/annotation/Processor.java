package nl.makertim.functionalmaker.annotation;

public interface Processor {
	Processor instance = new SingletonProcessor();

	Object getSingleton(Class<?> type);

	void injectStatic(Class<?> injectClass);

	void injectInstance(Object instance);

}
