package nl.makertim.functionalmaker.annotation;

public interface SingletonUserInterface {

	default void register() {
		Processor.instance.injectInstance(this);
	}
}
