package nl.makertim.functionalmaker.annotation;

public class SingletonUserExample implements SingletonUserInterface {

	static {
		Processor.instance.injectStatic(SingletonUserExample.class);
	}

	public SingletonUserExample() {
		Processor.instance.injectInstance(this);
	}
}
