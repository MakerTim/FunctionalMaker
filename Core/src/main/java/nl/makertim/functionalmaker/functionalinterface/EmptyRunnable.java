package nl.makertim.functionalmaker.functionalinterface;

public class EmptyRunnable implements Runnable {

	public static final EmptyRunnable DO_NOTHING = new EmptyRunnable();

	private EmptyRunnable() {
	}

	@Override
	public void run() {
		// does nothing
	}
}
