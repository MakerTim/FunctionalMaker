package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.IntPredicate;

@FunctionalInterface
public interface CatchIntPredicate<E extends Throwable> {

	boolean test(int value) throws E;

	default CatchIntPredicate<E> and(CatchIntPredicate<? extends E> other) {
		return value -> test(value) && other.test(value);
	}

	default CatchIntPredicate<E> and(IntPredicate other) {
		return value -> test(value) && other.test(value);
	}

	default CatchIntPredicate<E> negate() {
		return value -> !test(value);
	}

	default CatchIntPredicate<E> or(CatchIntPredicate<? extends E> other) {
		return value -> test(value) || other.test(value);
	}

	default CatchIntPredicate<E> or(IntPredicate other) {
		return value -> test(value) || other.test(value);
	}

	static <E extends Throwable> CatchIntPredicate<E> convert(IntPredicate predicate) {
		return predicate::test;
	}
}
