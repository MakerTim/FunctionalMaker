package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.DoublePredicate;

@FunctionalInterface
public interface CatchDoublePredicate<E extends Throwable> {

	boolean test(double value) throws E;

	default CatchDoublePredicate<E> and(CatchDoublePredicate<? extends E> other) {
		return value -> test(value) && other.test(value);
	}

	default CatchDoublePredicate<E> and(DoublePredicate other) {
		return value -> test(value) && other.test(value);
	}

	default CatchDoublePredicate<E> negate() {
		return value -> !test(value);
	}

	default CatchDoublePredicate<E> or(CatchDoublePredicate<? extends E> other) {
		return value -> test(value) || other.test(value);
	}

	default CatchDoublePredicate<E> or(DoublePredicate other) {
		return value -> test(value) || other.test(value);
	}

	static <E extends Throwable> CatchDoublePredicate<E> convert(DoublePredicate predicate) {
		return predicate::test;
	}
}
