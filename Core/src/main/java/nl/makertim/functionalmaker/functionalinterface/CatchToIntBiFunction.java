package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ToIntBiFunction;

@FunctionalInterface
public interface CatchToIntBiFunction<T, U, E extends Throwable> {

	int applyAsInt(T t, U u) throws E;

	static <T, U, E extends Throwable> CatchToIntBiFunction<T, U, E> convert(ToIntBiFunction<T, U> function) {
		return function::applyAsInt;
	}
}
