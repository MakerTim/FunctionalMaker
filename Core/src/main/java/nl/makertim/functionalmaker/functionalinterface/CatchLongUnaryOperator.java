package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.LongUnaryOperator;

@FunctionalInterface
public interface CatchLongUnaryOperator<E extends Throwable> {

	long applyAsLong(long operand) throws E;

	default CatchLongUnaryOperator<E> compose(CatchLongUnaryOperator<? extends E> before) {
		return (long v) -> applyAsLong(before.applyAsLong(v));
	}

	default CatchLongUnaryOperator<E> compose(LongUnaryOperator before) {
		return (long v) -> applyAsLong(before.applyAsLong(v));
	}

	default CatchLongUnaryOperator<E> andThen(CatchLongUnaryOperator<? extends E> after) {
		return (long t) -> after.applyAsLong(applyAsLong(t));
	}

	default CatchLongUnaryOperator<E> andThen(LongUnaryOperator after) {
		return (long t) -> after.applyAsLong(applyAsLong(t));
	}

	static CatchLongUnaryOperator identity() {
		return t -> t;
	}

	static <E extends Throwable> CatchLongUnaryOperator<E> convert(LongUnaryOperator operator) {
		return operator::applyAsLong;
	}
}
