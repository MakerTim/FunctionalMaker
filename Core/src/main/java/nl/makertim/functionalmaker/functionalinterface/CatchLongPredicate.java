package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.LongPredicate;

@FunctionalInterface
public interface CatchLongPredicate<E extends Throwable> {

	boolean test(long value) throws E;

	default CatchLongPredicate<E> and(CatchLongPredicate<? extends E> other) {
		return value -> test(value) && other.test(value);
	}

	default CatchLongPredicate<E> and(LongPredicate other) {
		return value -> test(value) && other.test(value);	}

	default CatchLongPredicate<E> negate() {
		return value -> !test(value);
	}

	default CatchLongPredicate<E> or(CatchLongPredicate<? extends E> other) {
		return value -> test(value) || other.test(value);
	}

	default CatchLongPredicate<E> or(LongPredicate other) {
		return value -> test(value) || other.test(value);
	}

	static <E extends Throwable> CatchLongPredicate<E> convert(LongPredicate predicate) {
		return predicate::test;
	}
}
