package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.BooleanSupplier;

@FunctionalInterface
public interface CatchBooleanSupplier<E extends Throwable> {

	boolean getAsBoolean() throws E;

	static <E extends Throwable> CatchBooleanSupplier<E> convert(BooleanSupplier supplier) {
		return supplier::getAsBoolean;
	}
}
