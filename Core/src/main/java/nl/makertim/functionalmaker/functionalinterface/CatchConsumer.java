package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.Consumer;

public interface CatchConsumer<T, E extends Throwable> {

	void accept(T t) throws E;

	default CatchConsumer<T, E> andThen(Consumer<? super T> after) {
		return (T t) -> {
			accept(t);
			after.accept(t);
		};
	}

	default CatchConsumer<T, E> andThen(CatchConsumer<? super T, ? extends E> after) {
		return (T t) -> {
			accept(t);
			after.accept(t);
		};
	}

	default CatchConsumer<T, E> before(Consumer<? super T> before) {
		return (T t) -> {
			before.accept(t);
			accept(t);
		};
	}

	default CatchConsumer<T, E> before(CatchConsumer<? super T, ? extends E> before) {
		return (T t) -> {
			before.accept(t);
			accept(t);
		};
	}

	static <T, E extends Throwable> CatchConsumer<T, E> convert(Consumer<T> consumer) {
		return consumer::accept;
	}
}
