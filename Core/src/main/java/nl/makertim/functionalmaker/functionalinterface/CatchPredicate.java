package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.Predicate;

@FunctionalInterface
public interface CatchPredicate<T, E extends Throwable> {

	boolean test(T t) throws E;

	default CatchPredicate<T, E> and(CatchPredicate<T, ? extends E> other) {
		return t -> test(t) && other.test(t);
	}

	default CatchPredicate<T, E> and(Predicate<T> other) {
		return t -> test(t) && other.test(t);
	}

	default CatchPredicate<T, E> negate() {
		return t -> !test(t);
	}

	default CatchPredicate<T, E> or(CatchPredicate<T, ? extends E> other) {
		return t -> test(t) || other.test(t);
	}

	static <T, E extends Throwable> CatchPredicate<T, E> convert(Predicate<T> predicate) {
		return predicate::test;
	}
}
