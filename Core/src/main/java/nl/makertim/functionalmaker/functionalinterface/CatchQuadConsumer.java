package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchQuadConsumer<T, U, V, W, E extends Throwable> {

	void accept(T t, U u, V v, W w) throws E;

	default CatchQuadConsumer<T, U, V, W, E> andThen(CatchQuadConsumer<? super T, ? super U, ? super V, ? super W, ? extends E> after) {
		return (l, r, v, w) -> {
			accept(l, r, v, w);
			after.accept(l, r, v, w);
		};
	}

	default CatchQuadConsumer<T, U, V, W, E> before(CatchQuadConsumer<? super T, ? super U, ? super V, ? super W, ? extends E> before) {
		return (l, r, v, w) -> {
			before.accept(l, r, v, w);
			accept(l, r, v, w);
		};
	}

	static <T, U, V, W, E extends Throwable> CatchQuadConsumer<T, U, V, W, E> convert(QuadConsumer<T, U, V, W> consumer) {
		return consumer::accept;
	}
}
