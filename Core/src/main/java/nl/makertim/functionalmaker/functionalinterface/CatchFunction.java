package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.Function;

@FunctionalInterface
public interface CatchFunction<T, U, E extends Throwable> {

	U apply(T t) throws E;

	default <R> CatchFunction<R, U, E> compose(Function<? super R, ? extends T> before) {
		return u -> apply(before.apply(u));
	}

	default <R> CatchFunction<R, U, E> compose(CatchFunction<? super R, ? extends T, ? extends E> before) {
		return u -> apply(before.apply(u));
	}

	default <R> CatchFunction<T, R, E> andThen(Function<? super U, ? extends R> after) {
		return t -> after.apply(apply(t));
	}

	default <R> CatchFunction<T, R, E> andThen(CatchFunction<? super U, ? extends R, ? extends E> after) {
		return t -> after.apply(apply(t));
	}

	static <T, E extends Throwable> CatchFunction<T, T, E> identity() {
		return t -> t;
	}

	static <T, U, E extends Throwable> CatchFunction<T, U, E> convert(Function<T, U> function) {
		return function::apply;
	}
}
