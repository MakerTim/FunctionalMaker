package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.IntToLongFunction;

@FunctionalInterface
public interface CatchIntToLongFunction<E extends Throwable> {

	long applyAsLong(int value) throws E;

	static <E extends Throwable> CatchIntToLongFunction<E> convert(IntToLongFunction function) {
		return function::applyAsLong;
	}
}
