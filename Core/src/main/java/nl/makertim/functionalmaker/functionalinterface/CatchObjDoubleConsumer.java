package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ObjDoubleConsumer;

@FunctionalInterface
public interface CatchObjDoubleConsumer<T, E extends Throwable> {

	void accept(T t, double value) throws E;

	static <T, E extends Throwable> CatchObjDoubleConsumer<T, E> convert(ObjDoubleConsumer<T> consumer) {
		return consumer::accept;
	}
}
