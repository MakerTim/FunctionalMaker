package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.IntToDoubleFunction;

@FunctionalInterface
public interface CatchIntToDoubleFunction<E extends Throwable> {

	double applyAsDouble(int value) throws E;

	static <E extends Throwable> CatchIntToDoubleFunction<E> convert(IntToDoubleFunction function) {
		return function::applyAsDouble;
	}
}
