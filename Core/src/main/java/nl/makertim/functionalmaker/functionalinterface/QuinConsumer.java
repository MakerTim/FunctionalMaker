package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface QuinConsumer<T, U, V, W, X> {

	void accept(T t, U u, V v, W w, X x);

	default QuinConsumer<T, U, V, W, X> andThen(QuinConsumer<? super T, ? super U, ? super V, ? super W, ? super X> after) {
		return (l, r, v, w, x) -> {
			accept(l, r, v, w, x);
			after.accept(l, r, v, w, x);
		};
	}

	default QuinConsumer<T, U, V, W, X> before(QuinConsumer<? super T, ? super U, ? super V, ? super W, ? super X> before) {
		return (l, r, v, w, x) -> {
			before.accept(l, r, v, w, x);
			accept(l, r, v, w, x);
		};
	}
}
