package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface QuinFunction<T, U, V, W, X, Y> {

	Y apply(T t, U u, V v, W w, X x);

}
