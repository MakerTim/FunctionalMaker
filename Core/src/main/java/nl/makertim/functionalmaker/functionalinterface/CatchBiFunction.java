package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.BiFunction;

@FunctionalInterface
public interface CatchBiFunction<T, U, V, E extends Throwable> {

	V apply(T t, U u) throws E;

	static <T, U, V, E extends Throwable> CatchBiFunction<T, U, V, E> convert(BiFunction<T, U, V> function) {
		return function::apply;
	}
}
