package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.IntSupplier;

@FunctionalInterface
public interface CatchIntSupplier<E extends Throwable> {

	int getAsInt() throws E;

	static <E extends Throwable> CatchIntSupplier<E> convert(IntSupplier supplier) {
		return supplier::getAsInt;
	}
}
