package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.LongFunction;

@FunctionalInterface
public interface CatchLongFunction<R, E extends Throwable> {

	R apply(long value) throws E;

	static <R, E extends Exception> CatchLongFunction<R, E> convert(LongFunction<R> function) {
		return function::apply;
	}
}
