package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchQuinPredicate<T, U, V, W, X, E extends Throwable> {

	boolean test(T t, U u, V v, W w, X x) throws E;

	default CatchQuinPredicate<T, U, V, W, X, E> and(QuinPredicate<? super T, ? super U, ? super V, ? super W, ? super X> other) {
		return (T t, U u, V v, W w, X x) -> test(t, u, v, w, x) && other.test(t, u, v, w, x);
	}

	default CatchQuinPredicate<T, U, V, W, X, E> and(CatchQuinPredicate<? super T, ? super U, ? super V, ? super W, ? super X, ? extends E> other) {
		return (T t, U u, V v, W w, X x) -> test(t, u, v, w, x) && other.test(t, u, v, w, x);
	}

	default CatchQuinPredicate<T, U, V, W, X, E> negate() {
		return (T t, U u, V v, W w, X x) -> !test(t, u, v, w, x);
	}

	default CatchQuinPredicate<T, U, V, W, X, E> or(QuinPredicate<? super T, ? super U, ? super V, ? super W, ? super X> other) {
		return (T t, U u, V v, W w, X x) -> test(t, u, v, w, x) || other.test(t, u, v, w, x);
	}

	default CatchQuinPredicate<T, U, V, W, X, E> or(CatchQuinPredicate<? super T, ? super U, ? super V, ? super W, ? super X, ? extends E> other) {
		return (T t, U u, V v, W w, X x) -> test(t, u, v, w, x) || other.test(t, u, v, w, x);
	}

	static <T, U, V, W, X, E extends Throwable> CatchQuinPredicate<T, U, V, W, X, E> convert(QuinPredicate<T, U, V, W, X> predicate) {
		return predicate::test;
	}
}
