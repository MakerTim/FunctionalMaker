package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchQuinFunction<T, U, V, W, X, Y, E extends Throwable> {

	Y apply(T t, U u, V v, W w, X x) throws E;

	static <T, U, V, W, X, Y, E extends Throwable> CatchQuinFunction<T, U, V, W, X, Y, E> convert(QuinFunction<T, U, V, W, X, Y> function) {
		return function::apply;
	}
}
