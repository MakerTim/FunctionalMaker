package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ToDoubleFunction;

@FunctionalInterface
public interface CatchToDoubleFunction<T, E extends Throwable> {

	double applyAsDouble(T value) throws E;

	static <T, E extends Throwable> CatchToDoubleFunction<T, E> convert(ToDoubleFunction<T> function) {
		return function::applyAsDouble;
	}
}
