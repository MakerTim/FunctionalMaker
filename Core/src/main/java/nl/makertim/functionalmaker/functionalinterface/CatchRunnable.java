package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchRunnable<E extends Throwable> {
	void run() throws E;

	static <E extends Throwable> CatchRunnable<E> convert(Runnable runnable) {
		return runnable::run;
	}
}
