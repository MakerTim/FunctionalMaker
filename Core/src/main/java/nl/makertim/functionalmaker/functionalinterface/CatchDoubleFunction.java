package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.DoubleFunction;

@FunctionalInterface
public interface CatchDoubleFunction<R, E extends Throwable> {

	R apply(double value) throws E;

	static <R, E extends Throwable> CatchDoubleFunction<R, E> convert(DoubleFunction<R> function) {
		return function::apply;
	}
}
