package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchTriPredicate<T, U, V, E extends Throwable> {

	boolean test(T t, U u, V v) throws E;

	default CatchTriPredicate<T, U, V, E> and(TriPredicate<? super T, ? super U, ? super V> other) {
		return (T t, U u, V v) -> test(t, u, v) && other.test(t, u, v);
	}

	default CatchTriPredicate<T, U, V, E> and(CatchTriPredicate<? super T, ? super U, ? super V, ? extends E> other) {
		return (T t, U u, V v) -> test(t, u, v) && other.test(t, u, v);
	}

	default CatchTriPredicate<T, U, V, E> negate() {
		return (T t, U u, V v) -> !test(t, u, v);
	}

	default CatchTriPredicate<T, U, V, E> or(TriPredicate<? super T, ? super U, ? super V> other) {
		return (T t, U u, V v) -> test(t, u, v) || other.test(t, u, v);
	}

	default CatchTriPredicate<T, U, V, E> or(CatchTriPredicate<? super T, ? super U, ? super V, ? extends E> other) {
		return (T t, U u, V v) -> test(t, u, v) || other.test(t, u, v);
	}

	static <T, U, V, E extends Throwable> CatchTriPredicate<T, U, V, E> convert(TriPredicate<T, U, V> predictate) {
		return predictate::test;
	}
}
