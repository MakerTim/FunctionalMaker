package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.LongConsumer;

@FunctionalInterface
public interface CatchLongConsumer<E extends Throwable> {

	void accept(long value) throws E;

	default CatchLongConsumer<E> andThen(CatchLongConsumer<? extends E> after) {
		return (long t) -> {
			accept(t);
			after.accept(t);
		};
	}

	default CatchLongConsumer<E> andThen(LongConsumer after) {
		return (long t) -> {
			accept(t);
			after.accept(t);
		};
	}

	static <E extends Throwable> CatchLongConsumer<E> convert(LongConsumer consumer) {
		return consumer::accept;
	}
}
