package nl.makertim.functionalmaker.functionalinterface;

import java.util.Collections;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

@FunctionalInterface
public interface CatchComparator<T, E extends Throwable> {

	int compare(T o1, T o2) throws E;

	default CatchComparator<T, E> thenComparing(CatchComparator<? super T, ? extends E> other) {
		return (c1, c2) -> {
			int res = compare(c1, c2);
			return (res != 0) ? res : other.compare(c1, c2);
		};
	}

	@SuppressWarnings("unchecked")
	default <U> CatchComparator<T, E> thenComparing(
			Function<? super T, ? extends U> keyExtractor,
			CatchComparator<? super U, ? extends E> keyComparator) {
		return thenComparing(comparing(keyExtractor, keyComparator));
	}

	@SuppressWarnings("unchecked")
	default <U extends Comparable<? super U>> CatchComparator<T, E> thenComparing(
			Function<? super T, ? extends U> keyExtractor) {
		return thenComparing(comparing(keyExtractor));
	}

	@SuppressWarnings("unchecked")
	default CatchComparator<T, E> thenComparingInt(ToIntFunction<? super T> keyExtractor) {
		return thenComparing(comparingInt(keyExtractor));
	}

	@SuppressWarnings("unchecked")
	default CatchComparator<T, E> thenComparingLong(ToLongFunction<? super T> keyExtractor) {
		return thenComparing(comparingLong(keyExtractor));
	}

	@SuppressWarnings("unchecked")
	default CatchComparator<T, E> thenComparingDouble(ToDoubleFunction<? super T> keyExtractor) {
		return thenComparing(comparingDouble(keyExtractor));
	}

	static <T extends Comparable<? super T>, E extends Throwable> CatchComparator<T, E> reverseOrder() {
		return convert(Collections.reverseOrder());
	}

	@SuppressWarnings("unchecked")
	static <T, U, E extends Throwable> CatchComparator<T, E> comparing(
			Function<? super T, ? extends U> keyExtractor,
			CatchComparator<? super U, E> keyComparator) {
		return (c1, c2) -> keyComparator.compare(keyExtractor.apply(c1),
				keyExtractor.apply(c2));
	}

	static <T, U extends Comparable<? super U>, E extends Throwable> CatchComparator<T, E> comparing(
			Function<? super T, ? extends U> keyExtractor) {
		return (c1, c2) -> keyExtractor.apply(c1).compareTo(keyExtractor.apply(c2));
	}

	static <T, E extends Throwable> CatchComparator<T, E> comparingInt(ToIntFunction<? super T> keyExtractor) {
		return (c1, c2) -> Integer.compare(keyExtractor.applyAsInt(c1), keyExtractor.applyAsInt(c2));
	}

	static <T, E extends Throwable> CatchComparator<T, E> comparingLong(ToLongFunction<? super T> keyExtractor) {
		return (c1, c2) -> Long.compare(keyExtractor.applyAsLong(c1), keyExtractor.applyAsLong(c2));
	}

	static <T, E extends Throwable> CatchComparator<T, E> comparingDouble(ToDoubleFunction<? super T> keyExtractor) {
		return (c1, c2) -> Double.compare(keyExtractor.applyAsDouble(c1), keyExtractor.applyAsDouble(c2));
	}

	static <T, E extends Throwable> CatchComparator<T, E> convert(Comparator<T> comparator) {
		return comparator::compare;
	}
}
