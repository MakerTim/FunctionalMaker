package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ObjLongConsumer;

@FunctionalInterface
public interface CatchObjLongConsumer<T, E extends Throwable> {

	void accept(T t, long value) throws E;

	static <T, E extends Throwable> CatchObjLongConsumer<T, E> convert(ObjLongConsumer<T> consumer) {
		return consumer::accept;
	}
}
