package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.DoubleBinaryOperator;

@FunctionalInterface
public interface CatchDoubleBinaryOperator<E extends Throwable> {

	double applyAsDouble(double a, double b) throws E;

	static <E extends Throwable> CatchDoubleBinaryOperator<E> convert(DoubleBinaryOperator doubleBinaryOperator) {
		return doubleBinaryOperator::applyAsDouble;
	}
}
