package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ToIntFunction;

@FunctionalInterface
public interface CatchToIntFunction<T, E extends Throwable> {

	int applyAsInt(T value) throws E;

	static <T, E extends Throwable> CatchToIntFunction<T, E> convert(ToIntFunction<T> function) {
		return function::applyAsInt;
	}

}
