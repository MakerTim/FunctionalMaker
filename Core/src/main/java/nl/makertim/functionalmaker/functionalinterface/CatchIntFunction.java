package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.IntFunction;

@FunctionalInterface
public interface CatchIntFunction<R, E extends Throwable> {

	R apply(int value) throws E;

	static <R, E extends Throwable> CatchIntFunction<R, E> convert(IntFunction<R> function) {
		return function::apply;
	}
}
