package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface TriConsumer<T, U, V> {

	void accept(T t, U u, V v);

	default TriConsumer<T, U, V> andThen(TriConsumer<? super T, ? super U, ? super V> after)  {
		return (l, r, v) -> {
			accept(l, r, v);
			after.accept(l, r, v);
		};
	}

	default TriConsumer<T, U, V> before(TriConsumer<? super T, ? super U, ? super V> before) {
		return (l, r, v) -> {
			before.accept(l, r, v);
			accept(l, r, v);
		};
	}
}
