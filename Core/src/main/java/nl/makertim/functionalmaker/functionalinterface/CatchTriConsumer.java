package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchTriConsumer<T, U, V, E extends Throwable> {

	void accept(T t, U u, V v) throws E;

	default CatchTriConsumer<T, U, V, E> andThen(CatchTriConsumer<? super T, ? super U, ? super V, ? extends E> after) {
		return (l, r, v) -> {
			accept(l, r, v);
			after.accept(l, r, v);
		};
	}

	default CatchTriConsumer<T, U, V, E> before(CatchTriConsumer<? super T, ? super U, ? super V, ? extends E> before) {
		return (l, r, v) -> {
			before.accept(l, r, v);
			accept(l, r, v);
		};
	}

	static <T, U, V, E extends Throwable> CatchTriConsumer<T, U, V, E> convert(TriConsumer<T, U, V> consumer) {
		return consumer::accept;
	}
}
