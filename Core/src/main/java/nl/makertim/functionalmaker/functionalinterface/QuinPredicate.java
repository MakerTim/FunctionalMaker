package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface QuinPredicate<T, U, V, W, X> {

	boolean test(T t, U u, V v, W w, X x);

	default QuinPredicate<T, U, V, W, X> and(QuinPredicate<? super T, ? super U, ? super V, ? super W, ? super X> other) {
		return (T t, U u, V v, W w, X x) -> test(t, u, v, w, x) && other.test(t, u, v, w, x);
	}

	default QuinPredicate<T, U, V, W, X> negate() {
		return (T t, U u, V v, W w, X x) -> !test(t, u, v, w, x);
	}

	default QuinPredicate<T, U, V, W, X> or(QuinPredicate<? super T, ? super U, ? super V, ? super W, ? super X> other) {
		return (T t, U u, V v, W w, X x) -> test(t, u, v, w, x) || other.test(t, u, v, w, x);
	}
}
