package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.BiConsumer;

@FunctionalInterface
public interface CatchBiConsumer<T, U, E extends Throwable> {

	void accept(T t, U u) throws E;

	default CatchBiConsumer<T, U, E> andThen(CatchBiConsumer<? super T, ? super U, ? extends E> after) {
		return (l, r) -> {
			accept(l, r);
			after.accept(l, r);
		};
	}

	default CatchBiConsumer<T, U, E> andThen(BiConsumer<? super T, ? super U> after) {
		return (l, r) -> {
			accept(l, r);
			after.accept(l, r);
		};
	}

	default CatchBiConsumer<T, U, E> before(CatchBiConsumer<? super T, ? super U, ? extends E> before) {
		return (l, r) -> {
			before.accept(l, r);
			accept(l, r);
		};
	}

	default CatchBiConsumer<T, U, E> before(BiConsumer<? super T, ? super U> before) {
		return (l, r) -> {
			before.accept(l, r);
			accept(l, r);
		};
	}

	static <T, U, E extends Throwable> CatchBiConsumer<T, U, E> convert(BiConsumer<T, U> consumer) {
		return consumer::accept;
	}
}
