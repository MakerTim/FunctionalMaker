package nl.makertim.functionalmaker.collection;

public class PrimitivesArray {

	private PrimitivesArray() {
	}

	public static boolean[] convert(Boolean[] obj) {
		boolean[] arrayPrimitive = new boolean[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static Boolean[] convert(boolean[] obj) {
		Boolean[] arrayPrimitive = new Boolean[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static boolean[][] convert(Boolean[][] obj) {
		boolean[][] arrayPrimitive = new boolean[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}

	public static Boolean[][] convert(boolean[][] obj) {
		Boolean[][] arrayPrimitive = new Boolean[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}


	public static byte[] convert(Byte[] obj) {
		byte[] arrayPrimitive = new byte[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static Byte[] convert(byte[] obj) {
		Byte[] arrayPrimitive = new Byte[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static byte[][] convert(Byte[][] obj) {
		byte[][] arrayPrimitive = new byte[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}

	public static Byte[][] convert(byte[][] obj) {
		Byte[][] arrayPrimitive = new Byte[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}


	public static char[] convert(Character[] obj) {
		char[] arrayPrimitive = new char[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static Character[] convert(char[] obj) {
		Character[] arrayPrimitive = new Character[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static char[][] convert(Character[][] obj) {
		char[][] arrayPrimitive = new char[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}

	public static Character[][] convert(char[][] obj) {
		Character[][] arrayPrimitive = new Character[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}


	public static short[] convert(Short[] obj) {
		short[] arrayPrimitive = new short[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static Short[] convert(short[] obj) {
		Short[] arrayPrimitive = new Short[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static short[][] convert(Short[][] obj) {
		short[][] arrayPrimitive = new short[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}

	public static Short[][] convert(short[][] obj) {
		Short[][] arrayPrimitive = new Short[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}


	public static int[] convert(Integer[] obj) {
		int[] arrayPrimitive = new int[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static Integer[] convert(int[] obj) {
		Integer[] arrayPrimitive = new Integer[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static int[][] convert(Integer[][] obj) {
		int[][] arrayPrimitive = new int[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}

	public static Integer[][] convert(int[][] obj) {
		Integer[][] arrayPrimitive = new Integer[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}


	public static long[] convert(Long[] obj) {
		long[] arrayPrimitive = new long[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static Long[] convert(long[] obj) {
		Long[] arrayPrimitive = new Long[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static long[][] convert(Long[][] obj) {
		long[][] arrayPrimitive = new long[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}

	public static Long[][] convert(long[][] obj) {
		Long[][] arrayPrimitive = new Long[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}


	public static float[] convert(Float[] obj) {
		float[] arrayPrimitive = new float[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static Float[] convert(float[] obj) {
		Float[] arrayPrimitive = new Float[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static float[][] convert(Float[][] obj) {
		float[][] arrayPrimitive = new float[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}

	public static Float[][] convert(float[][] obj) {
		Float[][] arrayPrimitive = new Float[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}


	public static double[] convert(Double[] obj) {
		double[] arrayPrimitive = new double[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static Double[] convert(double[] obj) {
		Double[] arrayPrimitive = new Double[obj.length];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = obj[i];
		}
		return arrayPrimitive;
	}

	public static double[][] convert(Double[][] obj) {
		double[][] arrayPrimitive = new double[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}

	public static Double[][] convert(double[][] obj) {
		Double[][] arrayPrimitive = new Double[obj.length][];
		for (int i = 0; i < obj.length; i++) {
			arrayPrimitive[i] = convert(obj[i]);
		}
		return arrayPrimitive;
	}
}
