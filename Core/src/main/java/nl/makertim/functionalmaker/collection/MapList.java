package nl.makertim.functionalmaker.collection;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MapList<T, L> extends LinkedHashMap<T, List<L>> {

	public List<L> append(T key, L value) {
		List<L> list = super.computeIfAbsent(key, k -> new ArrayList<>());
		list.add(value);
		return list;
	}

	public List<L> cutoff(T key, L value) {
		List<L> list = super.computeIfAbsent(key, k -> new ArrayList<>());
		list.remove(value);
		return list;
	}
}
