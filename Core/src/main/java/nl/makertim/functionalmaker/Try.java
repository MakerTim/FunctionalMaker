package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.functionalinterface.CatchIntSupplier;
import nl.makertim.functionalmaker.functionalinterface.CatchRunnable;
import nl.makertim.functionalmaker.functionalinterface.CatchSupplier;

import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Try {

	private Try() {
	}

	public static Tried execute(CatchRunnable<? extends Throwable> run) {
		Throwable throwable = null;
		try {
			run.run();
		} catch (Throwable ex) {
			throwable = ex;
		}
		return new Tried(throwable);
	}

	public static void executeAsync(CatchRunnable<? extends Throwable> run, ExecutorService threadPool, Consumer<Tried> result) {
		threadPool.submit(() -> result.accept(execute(run)));
	}

	public static <T> TriedWithData<T> execute(CatchSupplier<T, ? extends Throwable> supplier) {
		Throwable throwable = null;
		T data = null;
		try {
			data = supplier.get();
		} catch (Throwable ex) {
			throwable = ex;
		}
		return new TriedWithData<>(throwable, data);
	}

	public static TriedWithData<Integer> execute(CatchIntSupplier<? extends Throwable> supplier) {
		Throwable throwable = null;
		int data = 0;
		try {
			data = supplier.getAsInt();
		} catch (Throwable ex) {
			throwable = ex;
		}
		return new TriedWithData<>(throwable, data);
	}

	public static <T> void executeAsync(CatchSupplier<T, ? extends Throwable> supplier, ExecutorService threadPool, Consumer<TriedWithData<T>> result) {
		threadPool.submit(() -> result.accept(execute(supplier)));
	}


	public static class Tried {
		protected Throwable throwable;

		protected Tried(Throwable throwable) {
			this.throwable = throwable;
		}

		public Tried then(Runnable runnable) {
			if (throwable == null) {
				runnable.run();
			}
			return this;
		}

		public Tried orElse(Runnable runnable) {
			if (throwable != null) {
				runnable.run();
			}
			return this;
		}

		public Tried onFail(Consumer<Throwable> consumer) {
			if (throwable != null) {
				consumer.accept(throwable);
			}
			return this;
		}

		public <E extends Throwable> Tried orThrow(Function<Throwable, E> function) throws E {
			if (throwable != null) {
				throw function.apply(throwable);
			}
			return this;
		}

		public Tried always(Runnable runnable) {
			runnable.run();
			return this;
		}
	}

	public static class TriedWithData<T> extends Tried {
		protected T data;

		protected TriedWithData(Throwable throwable, T data) {
			super(throwable);
			this.data = data;
		}

		@Override
		public TriedWithData<T> then(Runnable runnable) {
			super.then(runnable);
			return this;
		}

		public TriedWithData<T> then(Consumer<T> consumer) {
			if (throwable == null) {
				consumer.accept(data);
			}
			return this;
		}

		@Override
		public TriedWithData<T> orElse(Runnable runnable) {
			super.orElse(runnable);
			return this;
		}

		public TriedWithData<T> orElse(Function<Throwable, T> consumer) {
			super.orElse(() -> {
				data = consumer.apply(throwable);
			});
			return this;
		}

		public TriedWithData<T> orElse(Supplier<T> consumer) {
			super.orElse(() -> {
				data = consumer.get();
			});
			return this;
		}

		@Override
		public TriedWithData<T> onFail(Consumer<Throwable> consumer) {
			if (throwable != null) {
				consumer.accept(throwable);
			}
			return this;
		}

		@Override
		public <E extends Throwable> TriedWithData<T> orThrow(Function<Throwable, E> function) throws E {
			if (throwable != null) {
				throw function.apply(throwable);
			}
			return this;
		}

		@Override
		public TriedWithData<T> always(Runnable runnable) {
			super.always(runnable);
			return this;
		}

		public T getData() {
			return data;
		}
	}
}
