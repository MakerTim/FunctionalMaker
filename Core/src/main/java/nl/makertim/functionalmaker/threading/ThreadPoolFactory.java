package nl.makertim.functionalmaker.threading;

import nl.makertim.functionalmaker.property.IntProperty;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPoolFactory implements ThreadFactory {

	private static final IntProperty poolNumber = new IntProperty(1);
	private final ThreadGroup group;
	private final AtomicInteger threadNumber = new AtomicInteger(1);
	private final String namePrefix;

	public ThreadPoolFactory() {
		SecurityManager s = System.getSecurityManager();
		group = (s != null) ? s.getThreadGroup() :
				Thread.currentThread().getThreadGroup();
		poolNumber.addOne();
		namePrefix = "pool-" +
				poolNumber.getValue() +
				"-thread-";
	}

	@Override
	public Thread newThread(Runnable r) {
		Thread t = new ThreadPoolThread(group, r,
				namePrefix + threadNumber.getAndIncrement(),
				0);
		if (t.isDaemon())
			t.setDaemon(false);
		if (t.getPriority() != Thread.NORM_PRIORITY)
			t.setPriority(Thread.NORM_PRIORITY);
		return t;
	}

	private static class ThreadPoolThread extends Thread {
		public ThreadPoolThread() {
		}

		public ThreadPoolThread(Runnable target) {
			super(target);
		}

		public ThreadPoolThread(ThreadGroup group, Runnable target) {
			super(group, target);
		}

		public ThreadPoolThread(String name) {
			super(name);
		}

		public ThreadPoolThread(ThreadGroup group, String name) {
			super(group, name);
		}

		public ThreadPoolThread(Runnable target, String name) {
			super(target, name);
		}

		public ThreadPoolThread(ThreadGroup group, Runnable target, String name) {
			super(group, target, name);
		}

		public ThreadPoolThread(ThreadGroup group, Runnable target, String name, long stackSize) {
			super(group, target, name, stackSize);
		}
	}
}
