package nl.makertim.functionalmaker.threading;

import nl.makertim.functionalmaker.Try;

import java.util.Collection;
import java.util.Objects;
import java.util.function.*;

public class Sync {

	private Sync() {
	}

	public static void waitFor(IntSupplier intSupplier, int min, int max) {
		waitFor(() -> {
			int supplier = intSupplier.getAsInt();
			return supplier >= min && supplier < max;
		});
	}

	public static void waitFor(IntSupplier intSupplier, int expected) {
		waitFor(() -> intSupplier.getAsInt() == expected);
	}

	public static void waitFor(DoubleSupplier intSupplier, double min, double max) {
		waitFor(() -> {
			double supplier = intSupplier.getAsDouble();
			return supplier >= min && supplier < max;
		});
	}

	public static void waitFor(DoubleSupplier intSupplier, double expected) {
		waitFor(() -> intSupplier.getAsDouble() == expected);
	}

	public static void waitFor(LongSupplier intSupplier, long min, long max) {
		waitFor(() -> {
			long supplier = intSupplier.getAsLong();
			return supplier >= min && supplier < max;
		});
	}

	public static void waitFor(LongSupplier intSupplier, long expected) {
		waitFor(() -> intSupplier.getAsLong() == expected);
	}

	public static <T> void waitFor(Supplier<T> supplier, T expected) {
		waitFor(() -> Objects.equals(supplier.get(), expected));
	}

	public static <T> void waitForNonNull(Supplier<T> supplier) {
		waitFor(() -> Objects.nonNull(supplier.get()));
	}

	public static <T> void waitForAdditionCollection(Collection<T> collection) {
		waitForAdditionCollection(collection, 50);
	}

	public static <T> void waitForAdditionCollection(Collection<T> collection, int timeBetween) {
		int initialSize;
		do {
			initialSize = collection.size();
			Try.execute(() -> Thread.sleep(timeBetween)).onFail(Throwable::printStackTrace);
		} while (initialSize != collection.size());
	}

	public static void waitFor(BooleanSupplier readyCondition) {
		while (!readyCondition.getAsBoolean()) {
			Try.execute(() -> Thread.sleep(50)).onFail(Throwable::printStackTrace);
		}
	}
}
