package nl.makertim.functionalmaker.property;

import java.util.function.BooleanSupplier;

public class BooleanProperty implements BooleanSupplier {

	protected boolean value;

	public BooleanProperty(boolean value) {
		this.value = value;
	}

	public boolean getValue() {
		return value;
	}

	public void invert() {
		value = !value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	@Override
	public boolean getAsBoolean() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(getAsBoolean());
	}
}
