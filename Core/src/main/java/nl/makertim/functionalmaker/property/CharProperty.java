package nl.makertim.functionalmaker.property;

import java.util.function.Supplier;

public class CharProperty implements Supplier<Character> {

	protected char value;

	public CharProperty(char value) {
		this.value = value;
	}

	public char getValue() {
		return value;
	}

	public void setValue(char value) {
		this.value = value;
	}

	@Override
	public Character get() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}
}
