package nl.makertim.functionalmaker.property;

import java.util.function.Supplier;

public class ObjectProperty<T> implements Supplier<T> {

	protected T value;

	public ObjectProperty(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public boolean isNotNull() {
		return value != null;
	}

	public boolean isNull() {
		return value == null;
	}

	@Override
	public T get() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}
}
