package nl.makertim.functionalmaker.property;

import java.util.function.Supplier;

public class StringBuilderProperty implements Supplier<StringBuilder> {

	protected StringBuilder value;

	public StringBuilderProperty(StringBuilder value) {
		this.value = value;
	}

	public StringBuilderProperty(String value) {
		this.value = new StringBuilder(value);
	}

	public StringBuilder getValue() {
		return value;
	}

	public String getString() {
		return value.toString();
	}

	public int length() {
		return value.length();
	}

	public void append(String string) {
		value.append(string);
	}

	public void append(char c) {
		value.append(c);
	}

	public void append(Object obj) {
		value.append(obj.toString());
	}

	public void setValue(StringBuilder value) {
		this.value = value;
	}

	@Override
	public StringBuilder get() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}
}
