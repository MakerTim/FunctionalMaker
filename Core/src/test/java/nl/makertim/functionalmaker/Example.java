package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.functionalinterface.CatchFunction;
import nl.makertim.functionalmaker.property.IntProperty;
import nl.makertim.functionalmaker.property.ObjectProperty;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;

public class Example {

	@Test
	public void property() {
		// Properties are there so you can use them in and around functional enviroments
		// without the restriction that something had to be final in a way or an other

		ObjectProperty<String> string = new ObjectProperty<>("");
		IntProperty amount = new IntProperty(0);
		String[] tmp = new String[]{"En hallo allemaal", "Vandaag gaan we weer eens dit doen", "en ik heb er echt zin in", "okay lets go", "yea!"};
		Arrays.stream(tmp).forEach(tmpString -> {
			amount.setValue(amount.getValue() + 1);
			string.setValue(string.getValue() + tmpString + "\n");
		});

		IntProperty iProperty = new IntProperty(0);
		Arrays.stream(tmp).parallel().forEach(tmpString -> {
			// i++; <- cannot be done, because values should be final
			iProperty.addOne();
		});
	}

	@Test
	public void functionalCatchers() {
		// Example where exception is thrown where its get called, so you can try/catch it there
		CatchFunction<File, Boolean, NullPointerException> run1 = Objects::isNull;

		// Example where exception is lost
		Function<File, Boolean> run2 = file -> {
			try {
				return Objects.isNull(file);
			} catch (NullPointerException ignore) {
				// ignore
			}
			return true;
		};
		// Example where exception is hidden
		Function<File, Boolean> run3 = file -> {
			try {
				return Objects.isNull(file);
			} catch (NullPointerException ex) {
				throw new NestedException(ex);
			}
		};

		// the difference lays in the implementation
		// where you CAN handle the npe from run1
		// but you can't from run2, because Consumer, or other functional interfaces CANT throw execptions
		// you CAN handle those with RuntimeException, but the IDE won't warn you about those
		// what leads to forgetting the handling of the exception, and worse code

		// run1
		try {
			run1.apply(null);
		} catch (NullPointerException ignore) {
			// ignore
		}
		// run2
		run2.apply(null);

		// run3
		try {
			run3.apply(null);
		} catch (NullPointerException re) {
			if (re.getCause() instanceof NullPointerException) {
				// dostuff
			}
		}
	}
}
