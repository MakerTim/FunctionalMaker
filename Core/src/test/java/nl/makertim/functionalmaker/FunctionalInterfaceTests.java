package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.functionalinterface.*;
import org.junit.Test;

@SuppressWarnings("CodeBlock2Expr")
public class FunctionalInterfaceTests {

	@Test
	public void testCatchBiConsumer() {
		CatchBiConsumer<Boolean, Integer, AssertionError> catchBiConsumer = (b, i) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchBiConsumer.accept(true, 1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchBiFunction() {
		CatchBiFunction<Integer, Integer, Boolean, AssertionError> catchBiFunction = (i1, i2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchBiFunction.apply(1, 2);
		}, AssertionError.class);
	}

	@Test
	public void testCatchBinaryOperator() {
		CatchBinaryOperator<Integer, AssertionError> catchBinaryOperator = (i1, i2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchBinaryOperator.apply(1, 2);
		}, AssertionError.class);
	}

	@Test
	public void testCatchBiPredicate() {
		CatchBiPredicate<Integer, Integer, AssertionError> catchBiPredicate = (i1, i2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchBiPredicate.test(1, 2);
		}, AssertionError.class);
	}

	@Test
	public void testCatchBooleanSupplier() {
		CatchBooleanSupplier<AssertionError> catchBooleanSupplier = () -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(catchBooleanSupplier::getAsBoolean, AssertionError.class);
	}

	@Test
	public void testCatchConsumer() {
		CatchConsumer<Integer, AssertionError> catchConsumer = x -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchConsumer.accept(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchDoubleBinaryOperator() {
		CatchDoubleBinaryOperator<AssertionError> catchDoubleBinaryOperator = (d1, d2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchDoubleBinaryOperator.applyAsDouble(1, 2);
		}, AssertionError.class);
	}

	@Test
	public void testCatchDoubleConsumer() {
		CatchDoubleConsumer<AssertionError> catchDoubleConsumer = d -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchDoubleConsumer.accept(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchDoubleFunction() {
		CatchDoubleFunction<Integer, AssertionError> catchDoubleFunction = d -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchDoubleFunction.apply(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchDoublePredicate() {
		CatchDoublePredicate<AssertionError> catchDoublePredicate = d -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchDoublePredicate.test(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchDoubleSupplier() {
		CatchDoubleSupplier<AssertionError> catchDoubleSupplier = () -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(catchDoubleSupplier::getAsDouble, AssertionError.class);
	}

	@Test
	public void testCatchDoubleToIntFunction() {
		CatchDoubleToIntFunction<AssertionError> catchDoubleToIntFunction = d -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchDoubleToIntFunction.applyAsInt(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchDoubleToLongFunction() {
		CatchDoubleToLongFunction<AssertionError> catchDoubleToLongFunction = d -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchDoubleToLongFunction.applyAsLong(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchDoubleUnaryOperator() {
		CatchDoubleUnaryOperator<AssertionError> catchDoubleUnaryOperator = d -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchDoubleUnaryOperator.applyAsDouble(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchFunction() {
		CatchFunction<Integer, Integer, AssertionError> catchFunction = i -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchFunction.apply(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchIntBinaryOperator() {
		CatchIntBinaryOperator<AssertionError> catchIntBinaryOperator = (i1, i2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchIntBinaryOperator.applyAsInt(1, 2);
		}, AssertionError.class);
	}

	@Test
	public void testCatchIntConsumer() {
		CatchIntConsumer<AssertionError> catchIntConsumer = i -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchIntConsumer.accept(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchIntFunction() {
		CatchIntFunction<Double, AssertionError> catchIntFunction = i -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchIntFunction.apply(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchIntPredicate() {
		CatchIntPredicate<AssertionError> catchIntPredicate = i -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchIntPredicate.test(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchIntSupplier() {
		CatchIntSupplier<AssertionError> catchIntSupplier = () -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(catchIntSupplier::getAsInt, AssertionError.class);
	}

	@Test
	public void testCatchIntToDoubleFunction() {
		CatchIntToDoubleFunction<AssertionError> catchIntToDoubleFunction = i -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchIntToDoubleFunction.applyAsDouble(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchIntToLongFunction() {
		CatchIntToLongFunction<AssertionError> catchIntToLongFunction = i -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchIntToLongFunction.applyAsLong(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchIntUnaryOperator() {
		CatchIntUnaryOperator<AssertionError> catchIntUnaryOperator = i -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchIntUnaryOperator.applyAsInt(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchLongBinaryOperator() {
		CatchLongBinaryOperator<AssertionError> catchLongBinaryOperator = (l1, l2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchLongBinaryOperator.applyAsLong(1, 2);
		}, AssertionError.class);
	}

	@Test
	public void testCatchLongConsumer() {
		CatchLongConsumer<AssertionError> catchLongConsumer = l -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchLongConsumer.accept(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchLongFunction() {
		CatchLongFunction<Boolean, AssertionError> catchLongFunction = l -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchLongFunction.apply(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchLongPredicate() {
		CatchLongPredicate<AssertionError> catchLongPredicate = l -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchLongPredicate.test(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchLongSupplier() {
		CatchLongSupplier<AssertionError> catchLongSupplier = () -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(catchLongSupplier::getAsLong, AssertionError.class);
	}

	@Test
	public void testCatchLongToDoubleFunction() {
		CatchLongToDoubleFunction<AssertionError> catchLongToDoubleFunction = l -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchLongToDoubleFunction.applyAsDouble(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchLongToIntFunction() {
		CatchLongToIntFunction<AssertionError> catchLongToIntFunction = l -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchLongToIntFunction.applyAsInt(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchLongUnaryOperator() {
		CatchLongUnaryOperator<AssertionError> catchLongUnaryOperator = l -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchLongUnaryOperator.applyAsLong(1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchObjDoubleConsumer() {
		CatchObjDoubleConsumer<Boolean, AssertionError> catchObjDoubleConsumer = (b, d) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchObjDoubleConsumer.accept(true, 1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchObjIntConsumer() {
		CatchObjIntConsumer<Boolean, AssertionError> catchObjIntConsumer = (b, i) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchObjIntConsumer.accept(false, 1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchObjLongConsumer() {
		CatchObjLongConsumer<Boolean, AssertionError> catchObjLongConsumer = (b, l) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchObjLongConsumer.accept(true, 1);
		}, AssertionError.class);
	}

	@Test
	public void testCatchPredicate() {
		CatchPredicate<Boolean, AssertionError> catchPredicate = b -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchPredicate.test(true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchQuadConsumer() {
		CatchQuadConsumer<Boolean, Boolean, Boolean, Boolean, AssertionError> catchQuadConsumer = (b1, b2, b3, b4) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchQuadConsumer.accept(true, false, true, false);
		}, AssertionError.class);
	}

	@Test
	public void testCatchQuadFunction() {
		CatchQuadFunction<Boolean, Boolean, Boolean, Boolean, Boolean, AssertionError> catchQuadFunction = (b1, b2, b3, b4) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchQuadFunction.apply(true, false, true, false);
		}, AssertionError.class);
	}

	@Test
	public void testCatchQuadPredicate() {
		CatchQuadPredicate<Boolean, Boolean, Boolean, Boolean, AssertionError> catchQuadPredicate = (b1, b2, b3, b4) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchQuadPredicate.test(true, false, true, false);
		}, AssertionError.class);
	}

	@Test
	public void testCatchQuinConsumer() {
		CatchQuinConsumer<Boolean, Boolean, Boolean, Boolean, Boolean, AssertionError> catchQuinConsumer = (b1, b2, b3, b4, b5) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchQuinConsumer.accept(true, false, true, false, true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchQuinFunction() {
		CatchQuinFunction<Boolean, Boolean, Boolean, Boolean, Boolean, Boolean, AssertionError> catchQuinFunction = (b1, b2, b3, b4, b5) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchQuinFunction.apply(true, false, true, false, true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchQuinPredicate() {
		CatchQuinPredicate<Boolean, Boolean, Boolean, Boolean, Boolean, AssertionError> catchQuinPredicate = (b1, b2, b3, b4, b5) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchQuinPredicate.test(true, false, true, false, true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchSupplier() {
		CatchSupplier<Boolean, AssertionError> catchSupplier = () -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(catchSupplier::get, AssertionError.class);
	}

	@Test
	public void testCatchToDoubleBiFunction() {
		CatchToDoubleBiFunction<Boolean, Boolean, AssertionError> catchToDoubleBiFunction = (b1, b2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchToDoubleBiFunction.applyAsDouble(true, false);
		}, AssertionError.class);
	}

	@Test
	public void testCatchToDoubleFunction() {
		CatchToDoubleFunction<Boolean, AssertionError> catchToDoubleFunction = b -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchToDoubleFunction.applyAsDouble(true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchToIntBiFunction() {
		CatchToIntBiFunction<Boolean, Boolean, AssertionError> catchToIntBiFunction = (b1, b2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchToIntBiFunction.applyAsInt(true, false);
		}, AssertionError.class);
	}

	@Test
	public void testCatchToIntFunction() {
		CatchToIntFunction<Boolean, AssertionError> catchToIntFunction = b -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchToIntFunction.applyAsInt(true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchToLongBiFunction() {
		CatchToLongBiFunction<Boolean, Boolean, AssertionError> catchToLongBiFunction = (b1, b2) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchToLongBiFunction.applyAsLong(true, false);
		}, AssertionError.class);
	}

	@Test
	public void testCatchToLongFunction() {
		CatchToLongFunction<Boolean, AssertionError> catchToLongFunction = b -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchToLongFunction.applyAsLong(true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchTriConsumer() {
		CatchTriConsumer<Boolean, Boolean, Boolean, AssertionError> catchTriConsumer = (b1, b2, b3) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchTriConsumer.accept(true, false, true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchTriFunction() {
		CatchTriFunction<Boolean, Boolean, Boolean, Boolean, AssertionError> catchTriFunction = (b1, b2, b3) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchTriFunction.apply(true, false, true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchTriPredicate() {
		CatchTriPredicate<Boolean, Boolean, Boolean, AssertionError> catchTriPredicate = (b1, b2, b3) -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchTriPredicate.test(true, false, true);
		}, AssertionError.class);
	}

	@Test
	public void testCatchUnaryOperator() {
		CatchUnaryOperator<Boolean, AssertionError> catchUnaryOperator = b -> {
			throw new AssertionError();
		};

		AssertTry.assumeThrows(() -> {
			catchUnaryOperator.apply(true);
		}, AssertionError.class);
	}

	@Test
	public void testQuadConsumer() {
		QuadConsumer<Boolean, Boolean, Boolean, Boolean> quadConsumer = (b1, b2, b3, b4) -> {
		};

		AssertTry.assumeSafe(() -> {
			quadConsumer.accept(true, false, true, false);
		});
	}

	@Test
	public void testQuadFunction() {
		QuadFunction<Boolean, Boolean, Boolean, Boolean, Boolean> quadFunction = (b1, b2, b3, b4) -> {
			return b1 && b2 && b3 && b4;
		};

		AssertTry.assumeSafe(() -> {
			quadFunction.apply(true, false, true, false);
		});
	}

	@Test
	public void testQuadPredicate() {
		QuadPredicate<Boolean, Boolean, Boolean, Boolean> quadPredicate = (b1, b2, b3, b4) -> {
			return true;
		};

		AssertTry.assumeSafe(() -> {
			quadPredicate.test(true, false, true, false);
		});
	}

	@Test
	public void testQuinConsumer() {
		QuinConsumer<Boolean, Boolean, Boolean, Boolean, Boolean> quinConsumer = (b1, b2, b3, b4, b5) -> {
		};

		AssertTry.assumeSafe(() -> {
			quinConsumer.accept(true, false, true, false, true);
		});
	}

	@Test
	public void testQuinFunction() {
		QuinFunction<Boolean, Boolean, Boolean, Boolean, Boolean, Integer> quinFunction = (b1, b2, b3, b4, b5) -> {
			return 1;
		};

		AssertTry.assumeSafe(() -> {
			quinFunction.apply(true, false, true, false, true);
		});
	}

	@Test
	public void testQuinPredicate() {
		QuinPredicate<Boolean, Boolean, Boolean, Boolean, Boolean> quinPredicate = (b1, b2, b3, b4, b5) -> {
			return true;
		};

		AssertTry.assumeSafe(() -> {
			quinPredicate.test(true, false, true, false, true);
		});
	}

	@Test
	public void testTriConsumer() {
		TriConsumer<Boolean, Boolean, Boolean> triConsumer = (b1, b2, b3) -> {
		};

		AssertTry.assumeSafe(() -> {
			triConsumer.accept(true, false, true);
		});
	}

	@Test
	public void testTriFunction() {
		TriFunction<Boolean, Boolean, Boolean, Integer> triFunction = (b1, b2, b3) -> {
			return 1;
		};

		AssertTry.assumeSafe(() -> {
			triFunction.apply(true, false, true);
		});
	}

	@Test
	public void testTriPredicate() {
		TriPredicate<Boolean, Boolean, Boolean> triPredicate = (b1, b2, b3) -> {
			return true;
		};

		AssertTry.assumeSafe(() -> {
			triPredicate.test(true, false, true);
		});
	}


}
